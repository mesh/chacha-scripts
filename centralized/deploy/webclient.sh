#!/bin/bash

#set -x		# TODO: uncomment for debug

# expects IP list of nodes to be monitored as command line argument ($@)

LIST=$@				# base CM IP list, sorted in ascending order of node IDs
ARR=(${LIST})		# base CM IP list as array

# ------------------------------------------------------------------------------

RUNS=50						# TODO: target number of successful monitoring cycles
MAX_PARALLEL_DL=0			# TODO: max. parallel nc instances that download CM data -> 0=unlimited
SUCCESS_CNT=0				# current count of successful runs
TIMEOUT=60					# netcat TCP connect/idle timeout in seconds

# TODO: specify HWMP root mode
# 0:	not a root node (purely reactive)
# 1/4:	root node, proactive RANN (heavy-weight, also seems buggy!)
# 2:	root node, proactive PREQ with no PREP (most light-weight, unidirectional)
# 3:	root node, proactive PREQ with PREP (medium-weight, bidirectional)
ROOTMODE=2

# ------------------------------------------------------------------------------

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=$ROOTMODE

# remove any previous 'monitoring.done' file
rm /run/monitoring.done 2>/dev/null

sleep 10	# TODO

i=1

while (( ${SUCCESS_CNT} < $RUNS )); do

	echo 3 > /proc/sys/vm/drop_caches

	mkdir -p /run/logs/Run_$(echo $i)

	# TODO: shuffle base CM IP array randomly
	ARR_SHUF=( $(shuf -e "${ARR[@]}") )

	# take absolute timestamp (date) before CM data retrieval
	TIMESTAMP1=$(date +%Y%m%d_%H-%M-%S-%3N)

	# perform concurrent CM data retrieval using xargs with max-procs limit
	# TODO: per job timestamp of netcat start
	for IP in ${ARR_SHUF[@]}; do echo $IP; done | xargs -I{} --max-procs ${MAX_PARALLEL_DL} bash -c "
	{ time nc -vv -n -q 0 -w $TIMEOUT {} 8080 &>/dev/null ; } 2>/run/logs/Run_$(echo $i)/time_netcat_node_{}.log;"
	status=$?
	SUCCESS=true
	if (($status > 0)); then
		SUCCESS=false
	fi

	# take absolute timestamp (date) after CM data retrieval
	TIMESTAMP2=$(date +%Y%m%d_%H-%M-%S-%3N)

	# create timestamp files
	touch /run/logs/Run_$(echo $i)/timestamp_1_startClusterDataRetrieve_$(echo $TIMESTAMP1)
	touch /run/logs/Run_$(echo $i)/timestamp_2_endClusterDataRetrieve_$(echo $TIMESTAMP2)

	if [[ "$SUCCESS" == true ]]; then
		SUCCESS_CNT=$((${SUCCESS_CNT}+1))
	else
		# rename log folder of unsuccessful run to denote an error
		mv /run/logs/Run_$(echo $i) /run/logs/Run_$(echo $i)_ERROR
	fi

	i=$(($i+1))

	sleep 10	# TODO

done

# after RUNS successful runs create "monitoring.done" file for measurement control
touch /run/monitoring.done

# deactivate rootmode again
iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=0

