#!/bin/bash

# runs on every CM, needs TestData file (fixed size random data) in path
# expects own mesh IF IP address as argument $1

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=0  # not a root node

mkdir -p /run/logs
mkdir -p /run/data

cp TestData /run/data/

echo 3 > /proc/sys/vm/drop_caches

{ tcpserver -vXHlRD -b 50 $1 8080 bash -c "cat /run/data/TestData" ; } 2>&1 | ts '[%Y-%m-%d %H:%M:%.S]' &>/run/logs/tcpserver_node_$(echo $1).log &
