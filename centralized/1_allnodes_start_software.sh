#!/bin/bash

# ------------------------------------------------------------------------------

# worst reference case 	# TODO: use following two lines for MAN1 case
#MAN="1"
#LIST="2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

# best reference case 	# TODO: use following two lines for MAN15 case
MAN="15"
LIST="1 2 3 4 5 7 8 9 10 11 13 14 16 17 19 20 21 22 23 25 26 27 28 29"

# ------------------------------------------------------------------------------

# kill any previous software instances
./2_allnodes_kill_software.sh

# kill any previous blockMyPeerLinks
./2_allnodes_kill_blockMyPeerLinks.sh

sleep 5		# TODO

# TODO: run blockMyPeerLinks
./1_allnodes_run_blockMyPeerLinks.sh

# restart ptpd
./1_allnodes_restart_ptpd_forced.sh

# clean remote log folders
./1_allnodes_cleanup_logs.sh

sleep 5		# TODO

# restart sshd
./1_allnodes_restart_sshd.sh

sleep 20	# TODO

for i in $LIST; do
	echo "Starting Web Server on Galileo $i ..."
	ssh root@"galileo$i" "cd CHaChA_centralized && nohup ./webserver.sh 192.168.123.$(($i+9)) &>/dev/null &" &
done

# create IP address list of nodes to be monitored
LIST_MAN=()
for i in $LIST; do
    LIST_MAN+=("192.168.123.$(($i+9))")
done
LIST_MAN_STR=$(echo ${LIST_MAN[@]})

echo "Starting Web Client on Galileo $MAN ..."
ssh root@"galileo$MAN" "cd CHaChA_centralized && nohup ./webclient.sh ${LIST_MAN_STR} &>/dev/null &" &

# periodically check for "monitoring.done" file on MAN
FILE="/run/monitoring.done"
DONE=false
while [[ "$DONE" == false ]]; do
	sleep 60	# TODO
	echo "Check for monitoring.done on Galileo $MAN ..."
	output=$(parallel --gnu --no-notice --nonall -S "root@galileo$MAN" "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
	#echo "output: $output"
	for i in $output; do
		if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
			echo "${i::-2} done"
			DONE=true
		else
			echo "${i::-2} not done"
			DONE=$DONE
		fi
	done
done

# kill blockMyPeerLinks after everything is done
./2_allnodes_kill_blockMyPeerLinks.sh

# kill software after everything is done
./2_allnodes_kill_software.sh

sleep 5		# TODO

# collect logs after everything is done
./3_allnodes_collect_logs.sh "$MAN"

