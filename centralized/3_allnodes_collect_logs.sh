#!/bin/bash

#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

MAN=$1

mkdir -p logs

# create subfolder with timestamp
TIMESTAMP=$(date +%Y%m%d_%H-%M-%S)
mkdir -p logs/${TIMESTAMP}_MAN${MAN}

for i in $LIST ; do

	echo "Collecting Log Files from Galileo $i ..."
    
	mkdir -p logs/${TIMESTAMP}_MAN${MAN}/Node_$i
        
	rsync -a root@"galileo$i":/run/logs/ logs/${TIMESTAMP}_MAN${MAN}/Node_$i &>/dev/null &

done
