#!/bin/bash

# ------------------------------------------------------------------------------

# TODO: list of all participating nodes
# LISTNR switches the different topologies
# LISTNR: 1..ring , 2..triangle , 3..L , 4..line , 5..plus , 6..allnodes

echo ""
echo "Possible topology-numbers: 1..ring , 2..triangle , 3..L , 4..line , 5..plus , 6..allnodes"
echo ""
read -p "Please type in a valid topology-number: " LISTNR
echo ""

while [ "$LISTNR" -lt 1 ] || [ "$LISTNR" -gt 6 ]; do
	
	echo "$LISTNR is not a valid topology-number!!!"
	echo ""
	echo "Possible topology-numbers: 1..ring , 2..triangle , 3..L , 4..line , 5..plus , 6..allnodes"
	echo ""
	read -p "Please type in a valid topology-number: " LISTNR
	echo ""

done

echo "$LISTNR is a valid topology-number"
echo ""


L5=("25 26 27 28 29")
L4=("19 20 21 22 23")
L3=("13 14 15 16 17")
L2=(" 7  8  9 10 11")
L1=(" 1  2  3  4  5")
ALLNODES="$L1 $L2 $L3 $L4 $L5"

case "$LISTNR" in

	1)		L5=("25 26 27 28 29")
			L4=("19          23")
			L3=("13          17")
			L2=(" 7          11")
			L1=(" 1  2  3  4  5")
			validOut="Ring";;

	2)		L5=("            29")
			L4=("         22 23")
			L3=("      15 16 17")
			L2=("    8  9 10 11")
			L1=(" 1  2  3  4  5")
			validOut="Triangle";;

	3)		L5=("25            ")
			L4=("19            ")
			L3=("13            ")
			L2=(" 7            ")
			L1=(" 1  2  3  4  5")
			validOut="L" ;;

	4)		L5=("              ")
			L4=("              ")
			L3=("              ")
			L2=("              ")
			L1=(" 1  2  3  4  5")
			validOut="Line" ;;

	5)		L5=("      27      ")
			L4=("      21      ")
			L3=("13 14 15 16 17")
			L2=("       9      ")
			L1=("       3      ")
			validOut="Plus" ;;

	6)		L5=("25 26 27 28 29")
			L4=("19 20 21 22 23")
			L3=("13 14 15 16 17")
			L2=(" 7  8  9 10 11")
			L1=(" 1  2  3  4  5")
			validOut="AllNodes";;
esac

echo "The CHaChA-algorithm will perform a $validOut-topology"
echo ""
read -p "How many Runs should CHaChA do ? >> " runsToPerform
echo ""
read -p "Is a reboot necessary ? (y/n) >> " rebootFlag
echo ""
read -p "Do you want to start now ? (y/n) >> " startCHaChA
echo ""

case "$startCHaChA" in
	Y|y|"") echo "Ok let's go" 
			echo "" ;;

	N|n) echo "Canceled by the user..."
		 echo ""
		 exit 1 ;;
esac

LIST="$L1 $L2 $L3 $L4 $L5"

# invLIST is the inverse of LIST 
invLIST=""
for i in $ALLNODES ;do
	
	denyFlag=0

	for j in $LIST ;do

		# if denyFlag is set to 1, the current LIST-entry will
		# not be added to the invLIST
		if [ $i -eq $j ]; then
			denyFlag=1
		fi

	done

	if [ $denyFlag == 0 ]; then
		invLIST="$invLIST $i"
	fi

done

#echo "ALLNODES" $ALLNODES
#echo "LIST" $LIST
#echo "invLIST" $invLIST

#echo  "Remaining seconds to start CHaChA:"
secs=$((100))
while [ $secs -gt 0 ]; do
   echo -ne "Restarting nodes... $secs seconds remaining \033[0K\r"
   sleep 1
   : $((secs--))
done

#./test_param.sh "$invLIST"
















