#!/bin/bash

#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 40"

for i in $LIST ; do

	echo "Killing Software on Galileo $i ..."

	ssh root@"galileo$i" 'pkill java ; pkill tcpdump ; pkill tcpserver ; pkill nc ; pkill sleep ; ps -ef | grep webclient | grep -v grep | awk '"'"'{print $2}'"'"' | xargs -r kill -9 ; iw dev mon0 del 2>/dev/null' &

done
