#!/bin/bash

#LIST=$(seq 1 1 40)
#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 40"

chmod a+x deploy/exec/*.sh

#for i in $LIST ; do
#
#	echo "Prepare Galileo $i ..."
#
#	ssh root@"galileo$i" "mkdir -p CHaChA_distributed" &
#
#done

#sleep 10

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a --delete deploy/ root@"galileo$i":/home/root/CHaChA_distributed/ &

done
