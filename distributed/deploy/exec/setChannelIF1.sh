#!/bin/bash

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
#source /home/root/phyNameDetectAndDevRename.sh		# TODO

DEVNAME_ATH=$1		# TODO: pass ifName directly to reduce execution time
IF1_IP=$2
FREQ=$3
MESHID=$4

./iw dev ${DEVNAME_ATH} mesh leave

# TODO: in general, power is kept between leave and join
#./iw dev ${DEVNAME_ATH} set txpower fixed 800		# MCS0
#./iw dev ${DEVNAME_ATH} set txpower fixed 1400		# MCS3

# TODO: MCS0 vs. MCS3 -> use rate that is achievable both by Atheros mPCIe and Ralink USB
#
#./iw dev ${DEVNAME_ATH} mesh join $MESHID freq $FREQ HT20 basic-rates 6 mcast-rate 6
#./iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 0 lgi-5
#
./iw dev ${DEVNAME_ATH} mesh join $MESHID freq $FREQ HT20 basic-rates 24 mcast-rate 24
./iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 3 lgi-5

# TODO: in general, IP address is kept between leave and join
#./ifconfig ${DEVNAME_ATH} ${IF1_IP} netmask 255.255.255.0 broadcast 192.168.123.255

./iw dev ${DEVNAME_ATH} set mesh_param mesh_plink_timeout=20

# TODO: after this ensure peer link blocking and set correct root mode
