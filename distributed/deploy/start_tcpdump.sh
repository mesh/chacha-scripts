#!/bin/bash

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

NAME=$(iam)
NR="${NAME//[^0-9]/}"

mkdir -p /run/logs
mkdir -p /run/data

# tcpdump options
# -nn: no dns/port lookups, -e: print link-level header, -H: .11s detection, -s: snaplen, -i: interface, -w: output file
# TODO: min. required snaplen to reliably gather tcp headers is >= 200 bytes (set with -s <size>)

# capture directly on (non-monitor) mesh vif
# TODO: capture on non-monitor mesh vif ONLY includes higher-layer traffic with local MAC as SRC/DST -> no MAC-layer (forwarding) info!
#tcpdump -nn -e -H -s 256 -i ${DEVNAME_ATH} -w /run/tcpdump_node_$NR.pcap &>/run/logs/tcpdump_node_$NR.log &

# capture on monitor vif created on mesh vif's PHY (set monitor mode with -I)
iw phy ${PHYNAME_ATH} interface add mon0 type monitor
iw dev mon0 set channel 149 HT20
ifconfig mon0 up
tcpdump -nn -e -H -s 256 -i mon0 -I -w /run/tcpdump_node_$NR.pcap &>/run/logs/tcpdump_node_$NR.log &

