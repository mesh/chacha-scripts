#!/bin/bash

# detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

NAME=$(iam)
NR="${NAME//[^0-9]/}"

iw ${DEVNAME_ATH} set mesh_param mesh_hwmp_rootmode=0  # start as non-root node


mkdir -p /run/logs
mkdir -p /run/data

echo 3 > /proc/sys/vm/drop_caches

# CLI args for CHaChA.jar: IF1name, IF1meshid, IF2name, IF2meshid, cfg-list "name=value" (see GlobalDefines.java)
# 
# default cfg:
# ---------------------------------
# SKIP_SETCHANNEL=false
# SKIP_MONITORING=false
#
# UPDATE_THREAD_PERIOD=10000
#
# NEIGH_INF_THREAD_SLEEP=5000
# INITIAL_THREAD_SLEEP=1000
# WAIT_MESHNODE_IS_FREE=100
#
# SYNC_BROADCAST_PERIOD=500
# SYNC_THRESHOLD=20
#
# NC_UNICAST_PERIOD=5000
#
# CH_BROADCAST_PERIOD=5000
# CH_NOTIFICATION_TIMEOUT=10000
#
# MASTER_PHASE_BROADCAST_PERIOD=500
# MASTER_PHASE_BROADCAST_TRIES=20
#
# PHASE0_DELAY=0
# PHASE1_DELAY=10000
# PHASE2_DELAY=10000
# PHASE3_DELAY=10000
# PHASE4_DELAY=10000
# PHASE5_DELAY=10000
# PHASE6_DELAY=0
# PHASE7_DELAY=0
#
# MONITORING_DELAY=20000	(20 seconds)
# ROAMING_DELAY=800000 (13,3 minutes / if -1 roaming will be skipped)
# ---------------------------------

# start with default cfg
#{ java -jar CHaChA.jar ${DEVNAME_ATH} mesh x x ; } &> /run/logs/CHaChA_node_$NR.log &

# TODO: set parameters as desired (SKIP_SETCHANNEL, SKIP_MONITORING)
# 1) log. clustering only ('dry' channel selection) -> true, true
# 2) log. clustering only + single-channel monitoring -> true, false
# 3) full clustering + multi-channel monitoring -> false, false
{ java -jar CHaChA.jar ${DEVNAME_ATH} mesh eth0 x x \
						SKIP_SETCHANNEL=true \
						SKIP_MONITORING=true \
						UPDATE_THREAD_PERIOD=1000 \
						NEIGH_INF_THREAD_SLEEP=1000 \
						INITIAL_THREAD_SLEEP=1000 \
						ROAMING_THREAD_SLEEP=2000 \
						WAIT_MESHNODE_IS_FREE=100 \
						SYNC_BROADCAST_PERIOD=500 \
						SYNC_THRESHOLD=10 \
						NC_UNICAST_PERIOD=2000 \
						CH_BROADCAST_PERIOD=2000 \
						CH_NOTIFICATION_TIMEOUT=5000 \
						MASTER_PHASE_BROADCAST_PERIOD=500 \
						MASTER_PHASE_BROADCAST_TRIES=10 \
						PHASE0_DELAY=0 \
						PHASE1_DELAY=0 \
						PHASE2_DELAY=2000 \
						PHASE3_DELAY=2000 \
						PHASE4_DELAY=2000 \
						PHASE5_DELAY=2000 \
						PHASE6_DELAY=0 \
						PHASE7_DELAY=0 \
						MONITORING_DELAY=10000 \
						ROAMING_DELAY=480000 \
						; } &> /run/logs/CHaChA_node_$NR.log &



