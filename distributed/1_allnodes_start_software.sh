#!/bin/bash

# ------------------------------------------------------------------------------

# TODO: list of all participating nodes
# LISTNR switches the different topologies
# LISTNR: 1..ring , 2..triangle , 3..L , 4..line , 5..plus , 6..allnodes

	# deploy software before starting it
	./0_allnodes_deploy_software.sh
	deplSecs=$((10))
	while [ $deplSecs -gt 0 ]; do
	  echo -ne "Deploying Software... $deplSecs seconds remaining \033[0K\r"
	  sleep 1
	  : $((deplSecs--))
	done
	echo "" 

echo ""
echo "Possible topology-numbers: 1..Ring , 2..Triangle , 3..L , 4..Line , 5..Plus , 6..AllNodes, 7...2x2, 8...3x3, 9...4x4"
echo ""
read -p "Please type in a valid topology-number: " LISTNR
echo ""

while [ "$LISTNR" -lt 1 ] || [ "$LISTNR" -gt 9 ]; do
	
	echo "$LISTNR is not a valid topology-number!!!"
	echo ""
	echo "Possible topology-numbers: 1..Ring , 2..Triangle , 3..L , 4..Line , 5..Plus , 6..AllNodes, 7...2x2, 8...3x3, 9...4x4"
	echo ""
	read -p "Please type in a valid topology-number: " LISTNR
	echo ""

done

echo "$LISTNR is a valid topology-number"
echo ""


L5=("25 26 27 28 29")
L4=("19 20 21 22 23")
L3=("13 14 15 16 17")
L2=(" 7  8  9 10 11")
L1=(" 1  2  3  4  5")
ALLNODES="$L1 $L2 $L3 $L4 $L5"

case "$LISTNR" in

	1)		L5=("25 26 27 28 29")
			L4=("19          23")
			L3=("13          17")
			L2=(" 7          11")
			L1=(" 1  2  3  4  5")
			validOut="Ring";;

	2)		L5=("            29")
			L4=("         22 23")
			L3=("      15 16 17")
			L2=("    8  9 10 11")
			L1=(" 1  2  3  4  5")
			validOut="Triangle";;

	3)		L5=("25            ")
			L4=("19            ")
			L3=("13            ")
			L2=(" 7            ")
			L1=(" 1  2  3  4  5")
			validOut="L" ;;

	4)		L5=("              ")
			L4=("              ")
			L3=("              ")
			L2=("              ")
			L1=(" 1  2  3  4  5")
			validOut="Line" ;;

	5)		L5=("      27      ")
			L4=("      21      ")
			L3=("13 14 15 16 17")
			L2=("       9      ")
			L1=("       3      ")
			validOut="Plus" ;;

	6)		L5=("25 26 27 28 29")
			L4=("19 20 21 22 23")
			L3=("13 14 15 16 17")
			L2=(" 7  8  9 10 11")
			L1=(" 1  2  3  4  5")
			validOut="AllNodes";;

	7)		L5=("              ")
			L4=("              ")
			L3=("              ")
			L2=(" 7  8         ")
			L1=(" 1  2         ")
			validOut="2x2";;

	8)		L5=("              ")
			L4=("              ")
			L3=("13 14 15      ")
			L2=(" 7  8  9      ")
			L1=(" 1  2  3      ")
			validOut="3x3";;

	9)		L5=("              ")
			L4=("19 20 21 22   ")
			L3=("13 14 15 16   ")
			L2=(" 7  8  9 10   ")
			L1=(" 1  2  3  4   ")
			validOut="4x4";;
esac

echo "The CHaChA-algorithm will perform a $validOut-topology"
echo ""
read -p "How many Runs should CHaChA do ? >> " runsToPerform
echo ""
read -p "Is a reboot necessary ? (y/n) >> " rebootNodes
echo ""
read -p "Should CHaChA perform a Roaming-Cycle ? (y/n) >> " performRoaming
echo ""
read -p "Do you want to start CHaChA now ? (y/n) >> " startCHaChA
echo ""

case "$startCHaChA" in
	Y|y|"") echo "Ok let's go" 
			echo "" ;;

	N|n) echo "Canceled by the user..."
		 echo ""
		 exit 1 ;;
esac

LIST=""

LIST="$L1 $L2 $L3 $L4 $L5"

# invLIST is the inverse of LIST 
invLIST=""
for i in $ALLNODES ;do
	
	denyFlag=0

	for j in $LIST ;do

		# if denyFlag is set to 1, the current LIST-entry will
		# not be added to the invLIST
		if [ $i -eq $j ]; then
			denyFlag=1
		fi

	done

	if [ $denyFlag == 0 ]; then
		invLIST="$invLIST $i"
	fi

done

#echo "ALLNODES" $ALLNODES
#echo "LIST" $LIST
#echo "invLIST" $invLIST


case "$rebootNodes" in
	Y|y|"") #TODO: perform a reboot of all nodes and wait 3 minutes for coming up

			./allnodes_reboot.sh #TODO: alle Knoten rebooten?

			secs=$((3 * 60))
			while [ $secs -gt 0 ]; do
			   echo -ne "Rebooting nodes... $secs seconds remaining \033[0K\r"
			   sleep 1
			   : $((secs--))
			done
			echo "" ;;

	N|n) echo "Starting without reboot"
		 ;;
esac

./4_allnodes_set_standard_peer_links.sh

# kill blockMyPeerLinks after everything is done
./1_allnodes_deactivate_meshIF.sh "$invLIST"

# TODO: de-/activate traffic capture (for CHaChA eval.)
CAPTURE_TRAFFIC=true

# TODO: predefined clusters to only evaluate monitoring (single-/multi-channel)
#
PREDEFINED_CLUSTERS=false
#
CH0="15" 	# Master CH
CH1="8"		# 1st aux CH
CH2="10"	# 2nd aux CH
CH3="20"	# 3rd aux CH
CH4="22"	# 4th aux CH

# TODO: roaming cycle to test the roaming functionality of CHaChA (duration to set in start_CHaChA-script)
#
ROAMING_CYCLE_AFTER_CLUSTERING=false
#
case "$performRoaming" in
	Y|y|"")  ROAMING_CYCLE_AFTER_CLUSTERING=true
			echo "" ;;

	N|n) ROAMING_CYCLE_AFTER_CLUSTERING=false
		 echo "";;
esac
# Cluster Constellation 1 (artificial example to test monitoring, 2018-02-14)
#CH0_LIST_CM="9 14 16 21"
#CH1_LIST_CM="1 2 3 7 13"
#CH2_LIST_CM="4 5 11"
#CH3_LIST_CM="19 25 26"
#CH4_LIST_CM="17 23 27 28 29"
#
# Cluster Constellation 2 (CHaChA without Master CH neighbour join preference, 2018-02-26)
#CH0_LIST_CM=""
#CH1_LIST_CM="1 2 7"
#CH2_LIST_CM="3 4 5 9 11 16 17"
#CH3_LIST_CM="13 14 19 21 25 26 27"
#CH4_LIST_CM="23 28 29"
#
# Cluster Constellation 3 (CHaChA with Master CH neighbour join preference, 2018-03-02)
#CH0_LIST_CM="9 14 16 21"
#CH1_LIST_CM="1 2 3 7"
#CH2_LIST_CM="4 5 11 17"
#CH3_LIST_CM="13 19 25 26 27"
#CH4_LIST_CM="23 28 29"
#
# Cluster Constellation 4 (CHaChA with Master CH neighbour join preference, 2018-03-07)
#CH0_LIST_CM="9 14 16 21"
#CH1_LIST_CM="1 2 3 7"
#CH2_LIST_CM="4 5 11"
#CH3_LIST_CM="13 19 25 26 27"
#CH4_LIST_CM="17 23 28 29"
#
# Cluster Constellation 5 (CHaChA with new WNPR, 2018-05-25)
CH0_LIST_CM="9 14 16 21"
CH1_LIST_CM="1 2 7"
CH2_LIST_CM="3 4 5 11 17"
CH3_LIST_CM="13 19 25 26 27"
CH4_LIST_CM="23 28 29"
#
# TODO: use only one (mPCIe) or two IF (mPCIe + USB) in predefined clusters case
# -> USB has performance issues on galileo boards but allows DL/UL overlap between different CH
# -> using only mPCIe IF requires CH channel switching between DL/UL in multi-channel case
# -> MULTI_INTERFACE and MULTI_CHANNEL + *_FREQ info need to be passed to webclient/webserver
MULTI_INTERFACE=false
#
# TODO: channels for predefined clusters case (if set to false only CC is used)
MULTI_CHANNEL=true
CH0_FREQ="5180"		# 36
CH1_FREQ="5200"		# 40
CH2_FREQ="5220"		# 44
CH3_FREQ="5240"		# 48
CH4_FREQ="5765"		# 153	TODO: 52 (DFS, TPC)
#
if [[ "${MULTI_INTERFACE}" == false ]]; then
	CH0_FREQ="5745"	# 149 (CC)
fi

# TODO: loop to automate multiple clustering runs
# -> then CHaChA.jar has to be started with all skip params set to "true"
# -> configure only 1 run if we evaluate monitoring for predefined clusters
RUNS_START=1	# 1
RUNS_END="$runsToPerform"	# TODO: 1 (predef. clust. monit. eval.) vs. X (clustering eval.)
RUN_LIST=$(seq ${RUNS_START} 1 ${RUNS_END})

# ------------------------------------------------------------------------------

for run in ${RUN_LIST}; do

	echo "Run $run of ${RUNS_END} ..."

	# kill any previous software instances
	./2_allnodes_kill_software.sh

	# kill any previous blockMyPeerLinks
	./2_allnodes_kill_blockMyPeerLinks.sh

	sleep 5		# TODO

	# TODO: run blockMyPeerLinks
	./1_allnodes_run_blockMyPeerLinks.sh

	# restart ptpd
	./1_allnodes_restart_ptpd_forced.sh

	# clean remote log folders
	./1_allnodes_cleanup_logs.sh

	sleep 5		# TODO

	# restart sshd
	./1_allnodes_restart_sshd.sh

	sleep 20	# TODO

	# PREDEFINED_CLUSTERS == false -> evaluate CHaChA (without monitoring)
	if [[ "${PREDEFINED_CLUSTERS}" == false ]]; then

		for i in $LIST; do

			echo "Starting Software on Galileo $i ..."

			if [[ "${CAPTURE_TRAFFIC}" == true ]]; then

				# tcpdump IF1 capture -> will be killed by java SW after complete clustering in phase 7
				ssh root@"galileo$i" "cd CHaChA_distributed && nohup ./start_tcpdump.sh &" &

			fi

			# start CHaChA (see script for cfg args)
			ssh root@"galileo$i" "cd CHaChA_distributed && nohup ./start_CHaChA.sh &" &

		done

	# PREDEFINED_CLUSTERS == true -> evaluate monitoring in predefined topology
	else

		# prepare IP address arguments

		CH0_IF1_IP=$(echo -e "192.168.123.$((${CH0}+9))")
		CH1_IF1_IP=$(echo -e "192.168.123.$((${CH1}+9))")
		CH2_IF1_IP=$(echo -e "192.168.123.$((${CH2}+9))")
		CH3_IF1_IP=$(echo -e "192.168.123.$((${CH3}+9))")
		CH4_IF1_IP=$(echo -e "192.168.123.$((${CH4}+9))")

		CH0_LIST_CM_IF1_IP="CM"
		for i in ${CH0_LIST_CM[*]}; do
			CH0_LIST_CM_IF1_IP="${CH0_LIST_CM_IF1_IP} 192.168.123.$(($i+9))"
		done

		CH1_LIST_CM_IF1_IP="CM"
		for i in ${CH1_LIST_CM[*]}; do
			CH1_LIST_CM_IF1_IP="${CH1_LIST_CM_IF1_IP} 192.168.123.$(($i+9))"
		done

		CH2_LIST_CM_IF1_IP="CM"
		for i in ${CH2_LIST_CM[*]}; do
			CH2_LIST_CM_IF1_IP="${CH2_LIST_CM_IF1_IP} 192.168.123.$(($i+9))"
		done

		CH3_LIST_CM_IF1_IP="CM"
		for i in ${CH3_LIST_CM[*]}; do
			CH3_LIST_CM_IF1_IP="${CH3_LIST_CM_IF1_IP} 192.168.123.$(($i+9))"
		done

		CH4_LIST_CM_IF1_IP="CM"
		for i in ${CH4_LIST_CM[*]}; do
			CH4_LIST_CM_IF1_IP="${CH4_LIST_CM_IF1_IP} 192.168.123.$(($i+9))"
		done

		# -> "aux" CH sync to Master CH only
		CH1_LIST_CH_IF1_IP="CH ${CH0_IF1_IP}"
		CH2_LIST_CH_IF1_IP="CH ${CH0_IF1_IP}"
		CH3_LIST_CH_IF1_IP="CH ${CH0_IF1_IP}"
		CH4_LIST_CH_IF1_IP="CH ${CH0_IF1_IP}"

		# -> Master CH has to know all "aux" CH (to check if complete info received per cycle)
		CH0_LIST_CH_IF1_IP="CH ${CH1_IF1_IP} ${CH2_IF1_IP} ${CH3_IF1_IP} ${CH4_IF1_IP}"

		# start webserver on all CM
		# args: * own IF1 IP
		# 		* MULTI_INTERFACE, MULTI_CHANNEL, *_FREQ

		for i in ${CH0_LIST_CM[*]}; do
			echo "Starting webserver on Galileo $i ..."
			CM_IF1_IP=$(echo -e "192.168.123.$(($i+9))")
			ssh root@"galileo$i" "cd CHaChA_distributed/exec && nohup ./webserver.sh ${CM_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH0_FREQ} mesh_${CH0_IF1_IP} &>/dev/null &" &
		done

		for i in ${CH1_LIST_CM[*]}; do
			echo "Starting webserver on Galileo $i ..."
			CM_IF1_IP=$(echo -e "192.168.123.$(($i+9))")
			ssh root@"galileo$i" "cd CHaChA_distributed/exec && nohup ./webserver.sh ${CM_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH1_FREQ} mesh_${CH1_IF1_IP} &>/dev/null &" &
		done

		for i in ${CH2_LIST_CM[*]}; do
			echo "Starting webserver on Galileo $i ..."
			CM_IF1_IP=$(echo -e "192.168.123.$(($i+9))")
			ssh root@"galileo$i" "cd CHaChA_distributed/exec && nohup ./webserver.sh ${CM_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH2_FREQ} mesh_${CH2_IF1_IP} &>/dev/null &" &
		done

		for i in ${CH3_LIST_CM[*]}; do
			echo "Starting webserver on Galileo $i ..."
			CM_IF1_IP=$(echo -e "192.168.123.$(($i+9))")
			ssh root@"galileo$i" "cd CHaChA_distributed/exec && nohup ./webserver.sh ${CM_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH3_FREQ} mesh_${CH3_IF1_IP} &>/dev/null &" &
		done

		for i in ${CH4_LIST_CM[*]}; do
			echo "Starting webserver on Galileo $i ..."
			CM_IF1_IP=$(echo -e "192.168.123.$(($i+9))")
			ssh root@"galileo$i" "cd CHaChA_distributed/exec && nohup ./webserver.sh ${CM_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH4_FREQ} mesh_${CH4_IF1_IP} &>/dev/null &" &
		done

		# start webclient on all CH
		# args: * own IF1 IP
		#		* MULTI_INTERFACE, MULTI_CHANNEL, *_FREQ
		# 		* IF1 IPs of CMs
		# 		* IF1 IP of Master CH (for "aux" CH) / IF1 IPs of remote CHs (for Master CH)

		echo "Starting webclient on Galileo $CH1, $CH2, $CH3, $CH4, and $CH0 (Master) ..."

		# TODO: webclient redirect stderr to /dev/null or log file
		REDIRECT="&>/dev/null"
#		REDIRECT="1>/dev/null 2>/run/logs/webclient_error.log"

		ssh root@"galileo$CH1" "cd CHaChA_distributed/exec && nohup ./webclient.sh ${CH1_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH1_FREQ} mesh_${CH1_IF1_IP} ${CH1_LIST_CM_IF1_IP} ${CH1_LIST_CH_IF1_IP} $REDIRECT &" &
		ssh root@"galileo$CH2" "cd CHaChA_distributed/exec && nohup ./webclient.sh ${CH2_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH2_FREQ} mesh_${CH2_IF1_IP} ${CH2_LIST_CM_IF1_IP} ${CH2_LIST_CH_IF1_IP} $REDIRECT &" &
		ssh root@"galileo$CH3" "cd CHaChA_distributed/exec && nohup ./webclient.sh ${CH3_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH3_FREQ} mesh_${CH3_IF1_IP} ${CH3_LIST_CM_IF1_IP} ${CH3_LIST_CH_IF1_IP} $REDIRECT &" &
		ssh root@"galileo$CH4" "cd CHaChA_distributed/exec && nohup ./webclient.sh ${CH4_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH4_FREQ} mesh_${CH4_IF1_IP} ${CH4_LIST_CM_IF1_IP} ${CH4_LIST_CH_IF1_IP} $REDIRECT &" &

		sleep 10 	# TODO

		# start Master CH last ("aux" CH already need to have their Ethernet IF tcpserver running to receive cycle start signal)
		ssh root@"galileo$CH0" "cd CHaChA_distributed/exec && nohup ./webclient.sh ${CH0_IF1_IP} ${MULTI_INTERFACE} ${MULTI_CHANNEL} ${CH0_FREQ} mesh_${CH0_IF1_IP} ${CH0_LIST_CM_IF1_IP} ${CH0_LIST_CH_IF1_IP} $REDIRECT &" &

	fi

	# periodically check for files "clustering.done" (1st) and "monitoring.done" (2nd)
	# if PREDEFINED_CLUSTERS == true (in this script)
	# -> no need to wait for "clustering.done"
	# -> "monitoring.done" will be written on Master CH after last cycle (remote CH info received)
	# if SKIP_MONITORING == true (CHAChA.jar arg)
	# -> Java SW will create "monitoring.done" directly after "clustering.done" (on Master CH)

	LOGINS=""
	for i in $LIST; do
		LOGINS="${LOGINS}root@galileo${i},"
	done

	if [[ "${PREDEFINED_CLUSTERS}" == false ]]; then

		# periodically check for "clustering.done" file
		FILE="/run/clustering.done"
		DONE=false
		while [[ "$DONE" == false ]]; do
			sleep 10	# TODO
			echo "Check for clustering.done ..."
			output=$(parallel --gnu --no-notice --nonall -S $LOGINS "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
			#echo "output: $output"
			DONE=true
			for i in $output; do
				if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
					#echo "${i::-2} done"
					DONE=$DONE
				else
					#echo "${i::-2} not done"
					DONE=false
				fi
			done
		done

	fi

	if [[ "${ROAMING_CYCLE_AFTER_CLUSTERING}" == true ]]; then

		# periodically check for "roaming.done" file
		FILE="/run/roaming.done"
		DONE=false
		while [[ "$DONE" == false ]]; do
			sleep 10	# TODO
			echo "Check for roaming.done ..."
			output=$(parallel --gnu --no-notice --nonall -S $LOGINS "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
			#echo "output: $output"
			DONE=true
			for i in $output; do
				if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
					#echo "${i::-2} done"
					DONE=$DONE
				else
					#echo "${i::-2} not done"
					DONE=false
				fi
			done
		done

	fi

	# periodically check for "monitoring.done" file
	FILE="/run/monitoring.done"
	DONE=false
	while [[ "$DONE" == false ]]; do
		echo "Check for monitoring.done ..."
		output=$(parallel --gnu --no-notice --nonall -S $LOGINS "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
		#echo "output: $output"
		for i in $output; do
			if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
				# assume DONE on first occurrence of the file (only created by Master CH)
				#echo "${i::-2} done"
				DONE=true
				break
			else
				#echo "${i::-2} not done"
				DONE=$DONE
			fi
		done
		if [[ "$DONE" == false ]]; then
			sleep 60	# TODO
		fi
	done

	# kill blockMyPeerLinks after everything is done
	./2_allnodes_kill_blockMyPeerLinks.sh

	# kill software after everything is done
	./2_allnodes_kill_software.sh

	sleep 5			# TODO

	# collect logs after everything is done
	./3_allnodes_collect_logs.sh "$validOut"

	sleep 5 		# TODO

	# give retrieval of pcap files more time
	if [[ "${CAPTURE_TRAFFIC}" == true ]]; then
		sleep 30	# TODO
	fi

	echo "CHaChA performed a $validOut-Topology with $runsToPerform Runs"

done

