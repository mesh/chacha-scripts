#!/bin/bash

L5=("25 26 27 28 29")
L4=("19 20 21 22 23")
L3=("13 14 15 16 17")
L2=(" 7  8  9 10 11")
L1=(" 1  2  3  4  5")
ALLNODES="$L1 $L2 $L3 $L4 $L5 40"

for i in $ALLNODES ; do

	echo "Blocking node $1 on Galileo $i..."

	ssh root@"galileo$i" "nohup ./blockMyPeerLinksWithArgument.sh $1 &>/dev/null &" &

done

