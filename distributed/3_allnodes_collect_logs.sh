#!/bin/bash

#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 40"

mkdir -p logs

# create subfolder with timestamp
TIMESTAMP=$(date +%Y%m%d_%H-%M-%S)
mkdir -p logs/1_clustering/$1/${TIMESTAMP}_DIST

for i in $LIST ; do

	echo "Collecting Log Files from Galileo $i ..."

	mkdir -p logs/1_clustering/$1/${TIMESTAMP}_DIST/Node_$i

	rsync -a root@"galileo$i":/run/logs/  logs/1_clustering/$1/${TIMESTAMP}_DIST/Node_$i &>/dev/null &
	rsync -a root@"galileo$i":/run/*.done logs/1_clustering/$1/${TIMESTAMP}_DIST/Node_$i &>/dev/null &
	rsync -a root@"galileo$i":/run/*.pcap logs/1_clustering/$1/${TIMESTAMP}_DIST/Node_$i &>/dev/null &

done
