#!/bin/bash

#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

for i in $LIST ; do

	echo "Cleanup Log Files on Galileo $i ..."

	ssh root@"galileo$i" "cd /run && rm -r logs/* data/* *.pcap *.done 2>/dev/null" &

done
