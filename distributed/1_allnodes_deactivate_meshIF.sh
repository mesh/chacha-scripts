#!/bin/bash

# all devices
#LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40"

# grid 5x5 (MeNTor, CHaChA, KadMesh)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"
LIST=$1

for i in $LIST ; do

	ssh root@"galileo$i" "/home/root/deactivate_meshIF.sh" &

done
