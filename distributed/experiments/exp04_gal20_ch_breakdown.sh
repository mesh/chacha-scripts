#!/bin/bash

"EXP04 - CH-Breakdown - GAL20"

echo "Wait 5 seconds before GAL20-Breakdown"
secs=$((5))
		while [ $secs -gt 0 ]; do
			echo -ne " $secs seconds remaining \033[0K\r"
			sleep 1
			: $((secs--))
			done
		echo ""

./4_allnodes_block_Arg_Neighbor.sh 20
./4_block_all_neighbors_on_arg_node.sh 20


echo "GAL20-Breakdown finished ... quitting experiment-script"
