#!/bin/bash

# ------------------------------------------------------------------------------

# kill blockMyPeerLinks after everything is done
#ssh root@"galileo40" "/home/root/deactivate_meshIF.sh" &

# TODO: de-/activate traffic capture (for CHaChA eval.)
CAPTURE_TRAFFIC=false

# TODO: predefined clusters to only evaluate monitoring (single-/multi-channel)
#
PREDEFINED_CLUSTERS=false
#
CH0="15" 	# Master CH
CH1="8"		# 1st aux CH
CH2="10"	# 2nd aux CH
CH3="20"	# 3rd aux CH
CH4="22"	# 4th aux CH

# TODO: roaming cycle to test the roaming functionality of CHaChA (duration to set in start_CHaChA-script)
#
ROAMING_CYCLE_AFTER_CLUSTERING=true
#

# Cluster Constellation 5 (CHaChA with new WNPR, 2018-05-25)
CH0_LIST_CM="9 14 16 21"
CH1_LIST_CM="1 2 7"
CH2_LIST_CM="3 4 5 11 17"
CH3_LIST_CM="13 19 25 26 27"
CH4_LIST_CM="23 28 29"
#
# TODO: use only one (mPCIe) or two IF (mPCIe + USB) in predefined clusters case
# -> USB has performance issues on galileo boards but allows DL/UL overlap between different CH
# -> using only mPCIe IF requires CH channel switching between DL/UL in multi-channel case
# -> MULTI_INTERFACE and MULTI_CHANNEL + *_FREQ info need to be passed to webclient/webserver
MULTI_INTERFACE=false
#
# TODO: channels for predefined clusters case (if set to false only CC is used)
MULTI_CHANNEL=true
CH0_FREQ="5180"		# 36
CH1_FREQ="5200"		# 40
CH2_FREQ="5220"		# 44
CH3_FREQ="5240"		# 48
CH4_FREQ="5765"		# 153	TODO: 52 (DFS, TPC)
#
if [[ "${MULTI_INTERFACE}" == false ]]; then
	CH0_FREQ="5745"	# 149 (CC)
fi

# TODO: loop to automate multiple clustering runs
# -> then CHaChA.jar has to be started with all skip params set to "true"
# -> configure only 1 run if we evaluate monitoring for predefined clusters
RUNS_START=1	# 1
RUNS_END=1	# TODO: 1 (predef. clust. monit. eval.) vs. X (clustering eval.)
RUN_LIST=$(seq ${RUNS_START} 1 ${RUNS_END})

# ------------------------------------------------------------------------------

	# kill any previous software instances
	ssh root@"galileo40" 'pkill java ; pkill tcpdump ; pkill tcpserver ; pkill nc ; pkill sleep ; ps -ef | grep webclient | grep -v grep | awk '"'"'{print $2}'"'"' | xargs -r kill -9 ; iw dev mon0 del 2>/dev/null' &

	# set StandardPeerLinks: GAL40 has no connection to any node
	#ssh root@"galileo40" "nohup ./StandardPeerLinks.sh &>/dev/null &" &

	sleep 2		# TODO

	# TODO: run blockMyPeerLinks
	ssh root@"galileo40" "nohup ./blockMyPeerLinks.sh &>/dev/null &" &

	# restart ptpd
	ssh root@"galileo40" "pkill -KILL ptpd && ptpd -c /home/root/ptp_slave.conf" &>/dev/null &

	# clean remote log folders
	ssh root@"galileo40" "cd /run && rm -r logs/* data/* *.pcap *.done 2>/dev/null" &

	sleep 2		# TODO

	# restart sshd
	# ssh root@"galileo40" "service ssh restart" &

	sleep 2	# TODO

	if [[ "${CAPTURE_TRAFFIC}" == true ]]; then

		# tcpdump IF1 capture -> will be killed by java SW after complete clustering in phase 7
		ssh root@"galileo40" "cd CHaChA_distributed && nohup ./start_tcpdump.sh &" &

	fi

	ssh root@"galileo40" "cd CHaChA_distributed && nohup ./start_CHaChA.sh &" &


	# periodically check for files "clustering.done" (1st) and "monitoring.done" (2nd)
	# if PREDEFINED_CLUSTERS == true (in this script)
	# -> no need to wait for "clustering.done"
	# -> "monitoring.done" will be written on Master CH after last cycle (remote CH info received)
	# if SKIP_MONITORING == true (CHAChA.jar arg)
	# -> Java SW will create "monitoring.done" directly after "clustering.done" (on Master CH)

	LOGINS=""
	
	LOGINS="${LOGINS}root@galileo40,"


	#if [[ "${PREDEFINED_CLUSTERS}" == false ]]; then

		# periodically check for "clustering.done" file
	#	FILE="/run/clustering.done"
	#	DONE=false
	#	while [[ "$DONE" == false ]]; do
	#		sleep 10	# TODO
	#		echo "Check for clustering.done ..."
	#		output=$(parallel --gnu --no-notice --nonall -S $LOGINS "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
			#echo "output: $output"
	#		DONE=true
	#		for i in $output; do
	#			if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
	#				#echo "${i::-2} done"
	#			DONE=$DONE
	#			else
					#echo "${i::-2} not done"
	#				DONE=false
	#			fi
	#		done
	#	done

	#fi

	#if [[ "${ROAMING_CYCLE_AFTER_CLUSTERING}" == true ]]; then

	#	# periodically check for "roaming.done" file
	#	FILE="/run/roaming.done"
	#	DONE=false
	#	while [[ "$DONE" == false ]]; do
	#		sleep 10	# TODO
	#		echo "Check for roaming.done ..."
	#		output=$(parallel --gnu --no-notice --nonall -S $LOGINS "test -e $FILE && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")
	#		#echo "output: $output"
	#		DONE=true
	#	for i in $output; do
	#			if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]]; then
	#				#echo "${i::-2} done"
	#				DONE=$DONE
	#			else
	#				#echo "${i::-2} not done"
	#				DONE=false
	#			fi
	#		done
	#	done

	#fi


	# kill blockMyPeerLinks after everything is done
	#ssh root@"galileo40" 'ps -ef | grep blockMyPeerLinks | grep -v grep | awk '"'"'{print $2}'"'"' | xargs -r kill -9' &>/dev/null &

	# kill software after everything is done
	#ssh root@"galileo40" 'pkill java ; pkill tcpdump ; pkill tcpserver ; pkill nc ; pkill sleep ; ps -ef | grep webclient | grep -v grep | awk '"'"'{print $2}'"'"' | xargs -r kill -9 ; iw dev mon0 del 2>/dev/null' &

	#sleep 5			# TODO

	# collect logs after everything is done
	#mkdir -p logs/1_clustering/$1/${TIMESTAMP}_DIST/Node_$40

	#rsync -a root@"galileo40":/run/logs/  logs/1_clustering/Gal40Results/${TIMESTAMP}_DIST/Node_40 &>/dev/null &
	#rsync -a root@"galileo40":/run/*.done logs/1_clustering/Gal40Results/${TIMESTAMP}_DIST/Node_40 &>/dev/null &
	#rsync -a root@"galileo40":/run/*.pcap logs/1_clustering/Gal40Results/${TIMESTAMP}_DIST/Node_40 &>/dev/null &

	#sleep 5 		# TODO

	# give retrieval of pcap files more time
	#if [[ "${CAPTURE_TRAFFIC}" == true ]]; then
	#	sleep 30	# TODO
	#fi

	#echo "ChaChA where performed correctly on Galileo 40... see Log-Files!"

