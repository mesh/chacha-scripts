#!/bin/bash
ROUND="2 3 4 5 11 17 23 29 28 27 26 25 19 13 7 1"

./4_unblock_gal40_on_arg_node.sh 1

./4_unblock_gal40_arg_peer.sh 1

echo "Link between Gal40 and Gal1 is ensured ..."

echo "Wait for 3 seconds"
secs=$((5))
		while [ $secs -gt 0 ]; do
			echo -ne " $secs seconds remaining \033[0K\r"
			sleep 1
			: $((secs--))
			done
		echo ""

echo "Start Software on GAL40"
./4_start_software_on_gal40.sh

echo "Wait for 60 seconds"
secs=$((60))
		while [ $secs -gt 0 ]; do
			echo -ne " $secs seconds remaining \033[0K\r"
			sleep 1
			: $((secs--))
			done
		echo ""

#tmpnode=1

echo "killing Link between Gal40 and Gal1 ..."

	./4_set_standard_peer_links_on_arg_node.sh 1
	./4_set_standard_peer_links_on_arg_node.sh 40

	#echo "Wait for 5 seconds"
	#secs=$((5))
	#	while [ $secs -gt 0 ]; do
	#		echo -ne " $secs seconds remaining \033[0K\r"
	#		sleep 1
	#		: $((secs--))
	#		done
	#	echo ""

sleep 5

#echo "killing Link between Gal40 and Gal1 ..."
#	./4_set_standard_peer_links_on_arg_node.sh 1
#	./4_set_standard_peer_links_on_arg_node.sh 40

for i in $ROUND ; do
	
	
	./4_unblock_gal40_on_arg_node.sh $i
	./4_unblock_gal40_arg_peer.sh $i

	echo "Link between Gal40 and Gal$i is ensured ..."

	echo "Wait for 20 seconds"
		secs=$((20))
			while [ $secs -gt 0 ]; do
				echo -ne " $secs seconds remaining \033[0K\r"
				sleep 1
				: $((secs--))
				done
			echo ""

	./4_set_standard_peer_links_on_arg_node.sh $i
	./4_set_standard_peer_links_on_arg_node.sh 40

	#echo "All Nodes Set StandardPeerLinks"
	#./4_allnodes_set_standard_peer_links.sh

	echo "killing Link between Gal40 and Gal$i ..."	

	#echo "Wait for 5 seconds"
	#secs=$((5))
	#	while [ $secs -gt 0 ]; do
	#		echo -ne " $secs seconds remaining \033[0K\r"
	#		sleep 1
	#		: $((secs--))
	#		done
	#	echo ""

sleep 5

	
	
#./4_set_standard_peer_links_on_arg_node.sh $tmpnode
	#./4_set_standard_peer_links_on_arg_node.sh 40


done

echo "Reached last Station for this experiment ... quitting experiment-script"



