#!/bin/bash

"EXP03 - CM-Breakdown - GAL1 + GAL21"

echo "Wait 5 seconds before GAL1-Breakdown"
secs=$((5))
		while [ $secs -gt 0 ]; do
			echo -ne " $secs seconds remaining \033[0K\r"
			sleep 1
			: $((secs--))
			done
		echo ""

./4_allnodes_block_Arg_Neighbor.sh 1
./4_block_all_neighbors_on_arg_node.sh 1

echo "Wait 20 seconds before GAL21-Breakdown"
secs=$((20))
		while [ $secs -gt 0 ]; do
			echo -ne " $secs seconds remaining \033[0K\r"
			sleep 1
			: $((secs--))
			done
		echo ""

./4_allnodes_block_Arg_Neighbor.sh 21
./4_block_all_neighbors_on_arg_node.sh 21

echo "GAL1-GAL21-Breakdown finished ... quitting experiment-script"
