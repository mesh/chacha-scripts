#!/bin/bash

#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 40"

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/blockMyPeerLinks.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/blockMyNeighbors.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/unblockMyNeighbors.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/unblockGalileo40.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/blockGalileo40.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/StandardPeerLinks.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/blockMyPeerLinksWithArgument.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/unblockMyPeerLinksWithArgument.sh root@"galileo$i":/home/root/ &

done

