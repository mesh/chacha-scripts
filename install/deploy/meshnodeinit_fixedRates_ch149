#!/bin/bash

set -x

export JAVA_HOME=/usr/java/jre1.8/bin/java
export PATH=$PATH:/usr/java/jre1.8/bin
export TZ="Europe/Berlin"


# stop unneeded services
#systemctl stop ofono.service
#systemctl stop galileo-sketch-reset.service
#systemctl stop galileo-target.service
systemctl stop systemd-timesyncd.service

# apply TCP/UDP send/receive socket buffer (rmem/wmem) optimizations
sysctl -w net.core.rmem_max=8388608						#->   8MB
sysctl -w net.core.wmem_max=8388608						#->   8MB
sysctl -w net.core.rmem_default=8388608					#->   8MB
sysctl -w net.core.wmem_default=8388608					#->   8MB
sysctl -w net.core.optmem_max=8388608					#->   8MB
sysctl -w net.ipv4.tcp_mem='8388608 8388608 8388608'	#->   8MB   8MB   8MB
sysctl -w net.ipv4.tcp_rmem='4096 87380 8388608'		#->   4kB  85kB	  8MB
sysctl -w net.ipv4.tcp_wmem='4096 65536 8388608'		#->   4kB  64kB	  8MB
sysctl -w net.ipv4.udp_mem='8388608 8388608 8388608'	#->   8MB   8MB   8MB
sysctl -w net.ipv4.udp_rmem_min=4096					#->   4kB
sysctl -w net.ipv4.udp_wmem_min=4096					#->   4kB
sysctl -w net.core.netdev_max_backlog=5000
sysctl -w net.ipv4.tcp_max_syn_backlog=5000

# TCP SYN/SYN-ACK timeout optimizations
sysctl -w net.ipv4.tcp_syn_retries=9		# 9 instead of 6 ((0s) 3s 6s 12s 24s 48s 96s 192s 384s 768s -> total 1533s)
sysctl -w net.ipv4.tcp_synack_retries=9		# 9 instead of 5 ((0s) 3s 6s 12s 24s 48s 96s 192s 384s 768s -> total 1533s)

# deactivate TCP metrics history
sysctl -w net.ipv4.tcp_no_metrics_save=1

# clear routing table cache
sysctl -w net.ipv4.route.flush=1

# deactivate TCP CUBIC Hystart mechanism
echo 0 > /sys/module/tcp_cubic/parameters/hystart
echo 0 > /sys/module/tcp_cubic/parameters/hystart_detect

# apply ARP cache optimizations
sysctl -w net.ipv4.neigh.default.base_reachable_time_ms=1200000		#-> 30 min instead of 30 sec
sysctl -w net.ipv4.neigh.default.mcast_solicit=25					#-> 25 instead of 3 multicast attempts
sysctl -w net.ipv4.neigh.default.ucast_solicit=25					#-> 25 instead of 3 unicast attempts


# initialize eth
ifconfig eth0 up
dhclient eth0

# forced ntp update
/usr/sbin/ntpd -gq

# autostart ptpd as slave
#ptpd2 -c /home/root/ptp_slave.conf	# yocto
ptpd -c /home/root/ptp_slave.conf	# debian

# start ftp server
#tcpsvd -vE 0.0.0.0 21 ftpd -w / &

# start rsync daemon
rsync --daemon


# initialize mesh IF - set antenna mapping, ARQ retry limit, TX power, channel, TX bitrate / MCS

#TODO: detect and export phyNames/devNames as env. var. (works for 1 and 2 phy)
source /home/root/phyNameDetectAndDevRename.sh

iw dev ${DEVNAME_ATH} set type mp

# set TX mask, RX mask -> 1=ant1, 2=ant2, 3=ant1+2
#iw phy ${PHYNAME_ATH} set antenna 1 1					# ath9k/AR9280: OK (TX=1 RX=1)
#iw phy ${PHYNAME_ATH} set antenna 2 2					# ath9k/AR9280: 'command failed: Invalid argument (-22)'
#iw phy ${PHYNAME_ATH} set antenna 1 2            		# ath9k/AR9280: leads to TX=1 RX=3
#iw phy ${PHYNAME_ATH} set antenna 2 1            		# ath9k/AR9280: leads to TX=2 RX=3
iw phy ${PHYNAME_ATH} set antenna 3 3					# ath9k/AR9280: OK (TX=3 RX=3)
#iw phy ${PHYNAME_ATH} set antenna 3 1            		# ath9k/AR9280: leads to TX=3 RX=3
#iw phy ${PHYNAME_ATH} set antenna 1 3            		# ath9k/AR9280: OK (TX=1 RX=3)
#iw phy ${PHYNAME_ATH} set antenna 3 2            		# ath9k/AR9280: leads to TX=3 RX=3
#iw phy ${PHYNAME_ATH} set antenna 2 3					# ath9k/AR9280: OK (TX=2 RX=3)

iw phy ${PHYNAME_ATH} set retry short 7 long 4			# initialize adapter with default (re-)try limits
#iw phy ${PHYNAME_ATH} set retry short 1 long 1			# use only 1 try without retries in 1st MRR chain segment
#iw phy ${PHYNAME_ATH} set retry short 10 long 10		# use (up to) 1 MRR chain segment  (ath9k max_rate_tries 10)
#iw phy ${PHYNAME_ATH} set retry short 20 long 20		# use (up to) 2 MRR chain segments (ath9k max_rate_tries 10)
#iw phy ${PHYNAME_ATH} set retry short 30 long 30		# use (up to) 3 MRR chain segments (ath9k max_rate_tries 10)
#iw phy ${PHYNAME_ATH} set retry short 15 long 15		# use (up to) 1 MRR chain segment  (ath9k max_rate_tries 15 -> HW capab.)
#iw phy ${PHYNAME_ATH} set retry short 30 long 30		# use (up to) 2 MRR chain segments (ath9k max_rate_tries 15 -> HW capab.)

#iw phy ${PHYNAME_ATH} set rts off						# deactivate RTS/CTS (default)
iw phy ${PHYNAME_ATH} set rts 500				    	# activate RTS/CTS (for frames >= 500 bytes)

ifconfig ${DEVNAME_ATH} up


iw dev ${DEVNAME_ATH} set power_save off				# deactivate power saving mechanism


# TX power (100 = 1dBm) -> 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 (1800 1900 2000)
# -> neighborhood distance depends on TX power and basic/mcast rates!
# -> to achieve both high link quality and small neighborhood, set high TX power and high basic / mcast rate
#iw dev ${DEVNAME_ATH} set txpower fixed 300
#iw dev ${DEVNAME_ATH} set txpower fixed 400
#iw dev ${DEVNAME_ATH} set txpower fixed 500
#iw dev ${DEVNAME_ATH} set txpower fixed 600
#iw dev ${DEVNAME_ATH} set txpower fixed 700
#iw dev ${DEVNAME_ATH} set txpower fixed 800
#iw dev ${DEVNAME_ATH} set txpower fixed 900
#iw dev ${DEVNAME_ATH} set txpower fixed 1000
#iw dev ${DEVNAME_ATH} set txpower fixed 1100
#iw dev ${DEVNAME_ATH} set txpower fixed 1200
#iw dev ${DEVNAME_ATH} set txpower fixed 1300
iw dev ${DEVNAME_ATH} set txpower fixed 1400
#iw dev ${DEVNAME_ATH} set txpower fixed 1500
#iw dev ${DEVNAME_ATH} set txpower fixed 1600
#iw dev ${DEVNAME_ATH} set txpower fixed 1700


#iw dev ${DEVNAME_ATH} set channel 6
#iw dev ${DEVNAME_ATH} set channel 6 HT20
#iw dev ${DEVNAME_ATH} set channel 6 HT40+

#iw dev ${DEVNAME_ATH} set channel 36
#iw dev ${DEVNAME_ATH} set channel 36 HT20
#iw dev ${DEVNAME_ATH} set channel 36 HT40+

#iw dev ${DEVNAME_ATH} set channel 60
#iw dev ${DEVNAME_ATH} set channel 60 HT20
#iw dev ${DEVNAME_ATH} set channel 60 HT40+

#iw dev ${DEVNAME_ATH} set channel 149
#iw dev ${DEVNAME_ATH} set channel 149 HT20
#iw dev ${DEVNAME_ATH} set channel 149 HT40+

#iw dev ${DEVNAME_ATH} set channel 159
#iw dev ${DEVNAME_ATH} set channel 159 HT20
#iw dev ${DEVNAME_ATH} set channel 159 HT40+

#iw dev ${DEVNAME_ATH} set channel 161
#iw dev ${DEVNAME_ATH} set channel 161 HT20
#iw dev ${DEVNAME_ATH} set channel 161 HT40+

#iw dev ${DEVNAME_ATH} set channel 165
#iw dev ${DEVNAME_ATH} set channel 165 HT20
#iw dev ${DEVNAME_ATH} set channel 165 HT40-


# set basic-rates & mcast-rate directly on MBSS join
# [ [freq <freq in MHz> <NOHT|HT20|HT40+|HT40-|80MHz>] [basic-rates <rate in Mbps,rate2,...>] [mcast-rate <rate in Mbps>] ]
# -> 802.11 a/g/n: 6 (9) 12 18 24 36 48 54 -> tune for 1-hop neighborhood @ certain TX power (also depends on MCS, HT20/40)
#iw dev ${DEVNAME_ATH} mesh join mesh


# Channel 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 2437 HT40+ basic-rates 48 mcast-rate 54


# Channel 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5180 HT40+ basic-rates 48 mcast-rate 54


# Channel 60
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5300 HT40+ basic-rates 48 mcast-rate 54


# Channel 149
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 6 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 12 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 18 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 NOHT basic-rates 54 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 6 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 12 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 18 mcast-rate 18
iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT20 basic-rates 54 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 6 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 12 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 18 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5745 HT40+ basic-rates 54 mcast-rate 54


# Channel 159
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5795 HT40+ basic-rates 48 mcast-rate 54


# Channel 161
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5805 HT40+ basic-rates 48 mcast-rate 54


# Channel 165
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40-

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 NOHT basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT20 basic-rates 48 mcast-rate 54

#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 24 mcast-rate 6
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 36 mcast-rate 12
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 48 mcast-rate 18
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 24 mcast-rate 24
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 36 mcast-rate 36
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 48 mcast-rate 48
#iw dev ${DEVNAME_ATH} mesh join mesh freq 5825 HT40- basic-rates 48 mcast-rate 54


# bitrates legacy-* -> 802.11 a/g/n:	6 (9) 12 18 24 36 48 54
# bitrates ht-mcs-* -> 802.11 n:	0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
# [sgi-2.4|lgi-2.4] [sgi-5|lgi-5] -> short/long guard interval (400/800ns)


#iw dev ${DEVNAME_ATH} set bitrates # set to defaults (RCA, minstrel)


#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 lgi-2.4
#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 sgi-2.4

#iw dev ${DEVNAME_ATH} set bitrates legacy-5 54 lgi-5
#iw dev ${DEVNAME_ATH} set bitrates legacy-5 54 sgi-5

#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 legacy-5 54 lgi-2.4 lgi-5
#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 legacy-5 54 sgi-2.4 sgi-5

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-2.4 4 lgi-2.4
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-2.4 4 sgi-2.4

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 4 lgi-5
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 4 sgi-5

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-2.4 4 ht-mcs-5 4 lgi-2.4 lgi-5
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-2.4 4 ht-mcs-5 4 sgi-2.4 sgi-5

#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 legacy-5 54 ht-mcs-2.4 4 ht-mcs-5 4 lgi-2.4 lgi-5
#iw dev ${DEVNAME_ATH} set bitrates legacy-2.4 54 legacy-5 54 ht-mcs-2.4 4 ht-mcs-5 4 sgi-2.4 sgi-5


# throughput in brackets: UDP, 17dB TXP, 30dB attenuators, MTL="Much Too Low" (http://mcsindex.com/)

# stable with attenuators

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 0 lgi-5	# (HT20: 4-5Mbps) (HT40: ~12Mbps)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 0 sgi-5	# (HT20: 4-5Mbps) (HT40: ~12Mbps)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 1 lgi-5	# (HT20: 7-8Mbps) (HT40: ~25Mbps)				-> 2nd best (HT40)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 1 sgi-5	# (HT20: 7-8Mbps) (HT40: ~25Mbps)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 2 lgi-5	# (HT20: ~17Mbps) (HT40: ~37Mbps, lossy)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 2 sgi-5	# (HT20: ~17Mbps) (HT40: ~37Mbps, lossy)

iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 3 lgi-5		# (HT20: ~22Mbps) (HT40: ~20Mbps, lossy, MTL)	-> 2nd best (HT20)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 3 sgi-5	# (HT20: ~22Mbps) (HT40: ~20Mbps, lossy, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 4 lgi-5	# (HT20: ~33Mbps) (HT40: ~19Mbps, MTL)			-> best (HT20)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 4 sgi-5	# (HT20: ~33Mbps) (HT40: ~19Mbps, MTL)

# unstable with attenuators

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 5 lgi-5	# (HT20: >30Mbps, unstable) (HT40: ~20Mbps, unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 5 sgi-5	# (HT20: >30Mbps, unstable) (HT40: ~20Mbps, unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 6 lgi-5	# (HT20: ~20Mbps, unstable, MTL) (HT40: <20Mbps, unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 6 sgi-5	# (HT20: ~20Mbps, unstable, MTL) (HT40: <20Mbps, unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 7 lgi-5	# (HT20: ~20Mbps, unstable, MTL) (HT40: unstable, MTL) # test IO bottleneck w/o attenuators -> 20Mbps !?
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 7 sgi-5	# (HT20: ~20Mbps, unstable, MTL) (HT40: unstable, MTL) # test IO bottleneck w/o attenuators -> 20Mbps !?

# ----------------------------------------------------------------------------------------------------------------------------------------

# stable with attenuators

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 8 lgi-5	# (HT20: ~11Mbps) (HT40: ~25Mbps)				-> best (HT40)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 8 sgi-5	# (HT20: ~11Mbps) (HT40: ~25Mbps)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 9 lgi-5	# (HT20: ~22Mbps) (HT40: ~20Mbps, lossy, MTL)	-> 2nd best (HT20)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 9 sgi-5	# (HT20: ~22Mbps) (HT40: ~20Mbps, lossy, MTL)

# unstable with attenuators

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 10 lgi-5	# (HT20: >30Mbps, unstable) (HT40: unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 10 sgi-5	# (HT20: >30Mbps, unstable) (HT40: unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 11 lgi-5	# (HT20: >35Mbps, unstable) (HT40: unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 11 sgi-5	# (HT20: >35Mbps, unstable) (HT40: unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 12 lgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 12 sgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 13 lgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 13 sgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 14 lgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 14 sgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL)

#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 15 lgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL) # test IO bottleneck w/o attenuators -> 20Mbps !?
#iw dev ${DEVNAME_ATH} set bitrates ht-mcs-5 15 sgi-5	# (HT20: unstable, MTL) (HT40: unstable, MTL) # test IO bottleneck w/o attenuators -> 20Mbps !?



#echo 0 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 1 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 2 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 3 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 4 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 5 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 6 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 7 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 8 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 9 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 10 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 11 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 12 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 13 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 14 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx
#echo 15 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/fixed_rate_idx

#check /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/netdev\:${DEVNAME_ATH}/
#rc_rateidx_mask_2ghz
#rc_rateidx_mask_5ghz
#rc_rateidx_mcs_mask_2ghz
#rc_rateidx_mcs_mask_5ghz



# set further debugfs variables
echo 0 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/limit_retry				# enforce userspace long/short (re-)try limits on ath9k+RC
#echo 1 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/limit_retry
echo 10 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/max_retry				# max. number of tries per MRR chain segment advertised to RC
#echo 15 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/max_retry
echo 6000 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/segment_size			# max. minstrel(_ht) MRR chain segment duration in us
#echo 1000000 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/segment_size
echo 1 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/rc/activate_RTS_CTS			# RTS/CTS for fallback rate and SMPS frames
echo 30 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/max_ba_sw_retries	# max. number of ath9k SW retries for AMPDU subframes
#echo 0 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/max_ba_sw_retries
#echo 1 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/disable_ani			# old kernels -> disable Atheros ANI
echo 0 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/ani 			    	# new kernels -> disable Atheros ANI
#echo -120 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override		# set fixed noise floor in dBm
echo -110 > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override		#TODO: overwritten individually below



# increase max. number of peer links to 50
iw dev ${DEVNAME_ATH} set mesh_param mesh_max_peer_links=50

# automatically remove inactive links from station list
iw dev ${DEVNAME_ATH} set mesh_param mesh_plink_timeout=20			# timeout in seconds

# only connect to peers that have average RSSI >= threshold #TODO: only for testing links after NF calib.
#iw dev ${DEVNAME_ATH} set mesh_param mesh_rssi_threshold=-85		# threshold in dBm (0 ... -255, 0=off)

# set further mesh peering protocol parameters #TODO
#iw dev ${DEVNAME_ATH} set mesh_param mesh_max_retries=20			#3
#iw dev ${DEVNAME_ATH} set mesh_param mesh_retry_timeout=1000		#100
#iw dev ${DEVNAME_ATH} set mesh_param mesh_confirm_timeout=1000		#100
#iw dev ${DEVNAME_ATH} set mesh_param mesh_holding_timeout=1000		#100



# add monitor IF for mesh IF
#iw dev ${DEVNAME_ATH} interface add mon0 type monitor
#ifconfig mon0 up
#tcpdump -i mon0 -s 65000 -p -U -w  /home/root/test.dump



# set mesh IP addr and noise floor individually for each node
#TODO: should use iam/address_list scripts and rely on eth0 MAC

#TODO: source NF definitions from external script
source /home/root/nf_list.sh

MAC="$(ifconfig ${DEVNAME_ATH} | grep wlan | cut -d' ' -f10 | tr '[:upper:]' '[:lower:]')"

case "$MAC" in

"04:f0:21:10:c3:23") # Galileo 1
	ip -4 addr add 192.168.123.10/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G1} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:20") # Galileo 2
	ip -4 addr add 192.168.123.11/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G2} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:0f") # Galileo 3
	ip -4 addr add 192.168.123.12/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G3} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:4f") # Galileo 4
	ip -4 addr add 192.168.123.13/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G4} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c2:90") # Galileo 5
	ip -4 addr add 192.168.123.14/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G5} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c2:8a") # Galileo 6
	ip -4 addr add 192.168.123.15/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G6} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:18") # Galileo 7
	ip -4 addr add 192.168.123.16/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G7} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:2b") # Galileo 8
	ip -4 addr add 192.168.123.17/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G8} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:15") # Galileo 9
	ip -4 addr add 192.168.123.18/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G9} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:57") # Galileo 10
	ip -4 addr add 192.168.123.19/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G10} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:41") # Galileo 11
	ip -4 addr add 192.168.123.20/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G11} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:71") # Galileo 12
	ip -4 addr add 192.168.123.21/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G12} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:2a") # Galileo 13
	ip -4 addr add 192.168.123.22/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G13} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:62") # Galileo 14
	ip -4 addr add 192.168.123.23/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G14} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:55") # Galileo 15
	ip -4 addr add 192.168.123.24/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G15} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c2:8e") # Galileo 16
	ip -4 addr add 192.168.123.25/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G16} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:0f:77:54") # Galileo 17
	ip -4 addr add 192.168.123.26/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G17} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:39") # Galileo 18
	ip -4 addr add 192.168.123.27/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G18} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:33") # Galileo 19
	ip -4 addr add 192.168.123.28/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G19} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c5:af") # Galileo 20
	ip -4 addr add 192.168.123.29/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G20} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:3f") # Galileo 21
	ip -4 addr add 192.168.123.30/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G21} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:51") # Galileo 22
	ip -4 addr add 192.168.123.31/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G22} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:4c") # Galileo 23
	ip -4 addr add 192.168.123.32/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G23} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c2:88") # Galileo 24
	ip -4 addr add 192.168.123.33/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G24} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:0d") # Galileo 25
	ip -4 addr add 192.168.123.34/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G25} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:53") # Galileo 26
	ip -4 addr add 192.168.123.35/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G26} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c5:52") # Galileo 27
	ip -4 addr add 192.168.123.36/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G27} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:17") # Galileo 28
	ip -4 addr add 192.168.123.37/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G28} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c5:51") # Galileo 29
	ip -4 addr add 192.168.123.38/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G29} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:59") # Galileo 30
	ip -4 addr add 192.168.123.39/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G30} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:52") # Galileo 31
	ip -4 addr add 192.168.123.40/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G31} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:54") # Galileo 32
	ip -4 addr add 192.168.123.41/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G32} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:38") # Galileo 33
	ip -4 addr add 192.168.123.42/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G33} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:65") # Galileo 34
	ip -4 addr add 192.168.123.43/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G34} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:21") # Galileo 35
	ip -4 addr add 192.168.123.44/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G35} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c5:53") # Galileo 36
	ip -4 addr add 192.168.123.45/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G36} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
#-------------------------------------------------------------------------------
"04:f0:21:10:c8:0c") # Galileo 37
	ip -4 addr add 192.168.123.46/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G37} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c3:3b") # Galileo 38
	ip -4 addr add 192.168.123.47/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G38} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:10:c8:0e") # Galileo 39
	ip -4 addr add 192.168.123.48/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G39} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
"04:f0:21:14:c6:5e") # Galileo 40
	ip -4 addr add 192.168.123.49/24 broadcast 192.168.123.255 dev ${DEVNAME_ATH}
	echo ${NF_G40} > /sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override
	;;
*)
	;;
esac

