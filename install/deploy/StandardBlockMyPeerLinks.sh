#!/bin/bash

#echo $$ > /home/root/blockMyPeerLinks_pid.tmp
#echo $BASHPID > /home/root/blockMyPeerLinks_bashpid.tmp

source /home/root/phyNameDetectAndDevRename.sh

myName=$(iam)
myNumber="${myName//Galileo/}"

	case "$myNumber" in

	"1") # Galileo 1

		# open: 1 2 7 8


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"2") # Galileo 2

		# open: 1 2 3 7 8 9


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"3") # Galileo 3

		# open: 2 3 4 8 9 10


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"4") # Galileo 4

		# open: 3 4 5 9 10 11


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"5") # Galileo 5

		# open: 4 5 6 10 11 12


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"6") # Galileo 6

		# open: 5 6 11 12


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"7") # Galileo 7

		# open: 1 2 7 8 13 14


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"8") # Galileo 8

		# open: 1 2 3 7 8 9 13 14 15


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"9") # Galileo 9

		# open: 2 3 4 8 9 10 14 15 16


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"10") # Galileo 10

		# open: 3 4 5 9 10 11 15 16 17


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"11") # Galileo 11

		# open: 4 5 6 10 11 12 16 17 18


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"12") # Galileo 12

		# open: 5 6 11 12 17 18


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"13") # Galileo 13

		# open: 7 8 13 14 19 20


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"14") # Galileo 14

		# open: 7 8 9 13 14 15 19 20 21


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"15") # Galileo 15

		# open: 8 9 10 14 15 16 20 21 22


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"16") # Galileo 16

		# open: 9 10 11 15 16 17 21 22 23


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"17") # Galileo 17

		# open: 10 11 12 16 17 18 22 23 24


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"18") # Galileo 18

		# open: 11 12 17 18 23 24


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"19") # Galileo 19

		# open: 13 14 19 20 25 26


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"20") # Galileo 20

		# open: 13 14 15 19 20 21 25 26 27


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"21") # Galileo 21

		# open: 14 15 16 20 21 22 26 27 28


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"22") # Galileo 22

		# open: 15 16 17 21 22 23 27 28 29


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"23") # Galileo 23

		# open: 16 17 18 22 23 24 28 29 30


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"24") # Galileo 24

		# open: 17 18 23 24 29 30


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"25") # Galileo 25

		# open: 19 20 25 26 31 32


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"26") # Galileo 26

		# open: 19 20 21 25 26 27 31 32 33


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"27") # Galileo 27

		# open: 20 21 22 26 27 28 32 33 34


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"28") # Galileo 28

		# open: 21 22 23 27 28 29 33 34 35


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"29") # Galileo 29

		# open: 22 23 24 28 29 30 34 35 36


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action open  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &

	
		;;

	"30") # Galileo 30

		# open: 23 24 29 30 35 36


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action open  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"31") # Galileo 31

		# open: 25 26 31 32


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"32") # Galileo 32

		# open: 25 26 27 31 32 33


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"33") # Galileo 33

		# open: 26 27 28 32 33 34


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"34") # Galileo 34

		# open: 27 28 29 33 34 35


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"35") # Galileo 35

		# open: 28 29 30 34 35 36


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action open  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"36") # Galileo 36

		# open: 29 30 35 36


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action open  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	"40") # Galileo 40

		# open: none


			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &

			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &


		;;

	*)
		;;

	esac



