#!/bin/bash

#set -x

source /home/root/phyNameDetectAndDevRename.sh

# used in CHaChA after clustering, IF2: USB dongles on common channel

# 3x3-node grid center of total 5x5-node grid:
#
#	20	21	22
#	14	15	16
#	8	9	10

# get vendor as 1st argument ('CSL' / 'ASUS' / 'BUFFALO')
# CSL:		RT5572
# ASUS:		RT3572
# BUFFALO:	RT3070
VENDOR=$1

# MAC addresses depending on vendor
G08_MAC=""
G09_MAC=""
G10_MAC=""
G14_MAC=""
G15_MAC=""
G16_MAC=""
G20_MAC=""
G21_MAC=""
G22_MAC=""

case $VENDOR in

	"CSL")

	G08_MAC="24:05:0f:e5:7e:af"
	G09_MAC="24:05:0f:e5:7e:ba"
	G10_MAC="24:05:0f:e5:7e:45"
	G14_MAC="24:05:0f:e5:7e:91"
	G15_MAC="24:05:0f:e5:76:b9"
	G16_MAC="24:05:0f:e5:7e:b8"
	G20_MAC="24:05:0f:e5:7e:ae"
	G21_MAC="24:05:0f:e5:7e:61"
	G22_MAC="24:05:0f:e5:7e:5f"

	;;

	"ASUS")

	G08_MAC="10:bf:48:5b:3d:3e"
	G09_MAC="10:bf:48:5b:3d:3f"
	G10_MAC="10:bf:48:5b:41:eb"
	G14_MAC="10:bf:48:5b:4a:66"
	G15_MAC="10:bf:48:5b:4a:c3"
	G16_MAC="10:bf:48:5b:4a:e8"
	G20_MAC="10:bf:48:5b:4a:fd"
	G21_MAC="10:bf:48:5b:62:04"
	G22_MAC="10:bf:48:5b:62:99"

	;;

	"BUFFALO")

	G08_MAC="10:6f:3f:a9:47:ce"
	G09_MAC="10:6f:3f:a9:47:69"
	G10_MAC="4c:e6:76:f1:ff:ce"
	G14_MAC="4c:e6:76:f1:fd:ca"
	G15_MAC="10:6f:3f:a9:47:9d"
	G16_MAC="10:6f:3f:a9:47:45"
	G20_MAC="4c:e6:76:f1:fe:32"
	G21_MAC="10:6f:3f:5b:d7:58"
	G22_MAC="10:6f:3f:a9:47:e6"

	;;

	*)	# default -> CSL

	G08_MAC="24:05:0f:e5:7e:af"
	G09_MAC="24:05:0f:e5:7e:ba"
	G10_MAC="24:05:0f:e5:7e:45"
	G14_MAC="24:05:0f:e5:7e:91"
	G15_MAC="24:05:0f:e5:76:b9"
	G16_MAC="24:05:0f:e5:7e:b8"
	G20_MAC="24:05:0f:e5:7e:ae"
	G21_MAC="24:05:0f:e5:7e:61"
	G22_MAC="24:05:0f:e5:7e:5f"

	;;

esac

myName=$(iam)

# periodically refresh desired neighborhood
while true ; do

	case "$myName" in

	"Galileo8")

			#iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action open  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action block  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action block  &	# G16
			iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action block  &	# G20
			iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action block  &	# G21
			iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action block  &	# G22

		;;

	"Galileo9")

			#iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action open  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			#iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action open  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action block  &	# G20
			iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action block  &	# G21
			iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action block  &	# G22

		;;

	"Galileo10")

			iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action block  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			#iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action open  &	# G10
			iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action block  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action block  &	# G20
			iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action block  &	# G21
			iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action block  &	# G22

		;;

	"Galileo14")

			#iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action open  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action block  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action block  &	# G16
			#iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action open  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action block  &	# G22

		;;

	"Galileo15")

			#iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action open  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			#iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action open  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			#iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action open  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			#iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action open  &	# G22

		;;

	"Galileo16")

			iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action block  &	# G8
			#iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action open  &	# G9
			#iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action open  &	# G10
			iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action block  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action block  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			#iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action open  &	# G22

		;;

	"Galileo20")

			iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action block  &	# G8
			iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action block  &	# G9
			iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action block  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action block  &	# G16
			#iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action open  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action block  &	# G22

		;;

	"Galileo21")

			iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action block  &	# G8
			iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action block  &	# G9
			iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action block  &	# G10
			#iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action open  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			#iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action open  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			#iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action open  &	# G22

		;;

	"Galileo22")

			iw dev ${DEVNAME_RAL} station set $G08_MAC plink_action block  &	# G8
			iw dev ${DEVNAME_RAL} station set $G09_MAC plink_action block  &	# G9
			iw dev ${DEVNAME_RAL} station set $G10_MAC plink_action block  &	# G10
			iw dev ${DEVNAME_RAL} station set $G14_MAC plink_action block  &	# G14
			#iw dev ${DEVNAME_RAL} station set $G15_MAC plink_action open  &	# G15
			#iw dev ${DEVNAME_RAL} station set $G16_MAC plink_action open  &	# G16
			iw dev ${DEVNAME_RAL} station set $G20_MAC plink_action block  &	# G20
			#iw dev ${DEVNAME_RAL} station set $G21_MAC plink_action open  &	# G21
			#iw dev ${DEVNAME_RAL} station set $G22_MAC plink_action open  &	# G22

		;;

	*)		# default
		;;

	esac

	sleep 20	# TODO

done


