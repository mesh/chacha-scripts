#!/bin/bash	

source /home/root/phyNameDetectAndDevRename.sh

myName=$(iam)
myNumber="${myName//Galileo/}"

case "$1" in		

"1") # Galileo 1
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action open  &
			echo "Galileo1 is unblocked ..."
			;;
"2") # Galileo 2
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action open  &
			;;
"3") # Galileo 3
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action open  &
			;;
"4") # Galileo 4
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action open  &
			;;
"5") # Galileo 5
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action open  &
			;;
"6") # Galileo 6
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action open  &
			;;
"7") # Galileo 7
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action open  &
			;;
"8") # Galileo 8
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action open  &
			;;
"9") # Galileo 9
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action open  &
			;;
"10") # Galileo 10
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action open  &
			;;
"11") # Galileo 11
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action open  &
			;;
"12") # Galileo 12
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action open  &
			;;
"13") # Galileo 13
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action open  &
			;;
"14") # Galileo 14
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action open  &
			;;
"15") # Galileo 15
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action open  &
			;;
"16") # Galileo 16
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action open  &
			;;
"17") # Galileo 17
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action open  &
			;;
"18") # Galileo 18
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action open  &
			;;
"19") # Galileo 19
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action open  &
			;;
"20") # Galileo 20
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action open  &
			;;
"21") # Galileo 21
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action open  &
			;;
"22") # Galileo 22
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action open  &
			;;
"23") # Galileo 23
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action open  &
			;;
"24") # Galileo 24
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action open  &
			;;
"25") # Galileo 25
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action open  &
			;;
"26") # Galileo 26
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action open  &
			;;
"27") # Galileo 27
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action open  &
			;;
"28") # Galileo 28
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action open  &
			;;
"29") # Galileo 29
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action open  &
			;;
"30") # Galileo 30
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action open  &
			;;
"31") # Galileo 31
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action open  &
			;;
"32") # Galileo 32
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action open  &
			;;
"33") # Galileo 33
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action open  &
			;;
"34") # Galileo 34
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action open  &
			;;
"35") # Galileo 35
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action open  &
			;;
"36") # Galileo 36
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action open  &
			;;

"40") # Galileo 40
			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action open  &
			;;
			*)
			;;

esac
