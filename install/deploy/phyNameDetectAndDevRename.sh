#!/bin/bash

#set -x

# TODO: script can only handle the following Galileo board configurations:
#	- only 1 atheros phy present (mPCIe)
#	- only 1 ralink phy present (mPCIe or USB)
#	- only 1 atheros (mPCIe) and 1 ralink phy (USB) present

phyAtheros=""		# will be determined if atheros adapter present
devAtheros=""		# will be determined if atheros adapter present

phyRalink=""		# will be determined if ralink adapter present
devRalink=""		# will be determined if ralink adapter present

debugfs="/sys/kernel/debug/ieee80211"

IFS=$'\n'			# string to array delimiter
phyArray=($(ls $debugfs))

for phyName in "${phyArray[@]}"; do

	if test -e "$debugfs/$phyName/ath9k"; then

		phyAtheros=$phyName

		# assume only 1 desired netdev per phy with name scheme "wlanX"
		# ignore any monitor interface (assume "mon" in name)
		devAtheros=$(ls "$debugfs/$phyName" | grep netdev:wlan | cut -d : -f 2)

		# TODO: renaming of netdev is problematic (driver-dependent)
#		if [[ "$devAtheros" != "" ]] && [[ "$devAtheros" != "wlan0" ]]; then
#			iw dev $devAtheros del
#		fi

	fi

	if test -e "$debugfs/$phyName/rt2800usb" || test -e "$debugfs/$phyName/rt2800pci"; then

		phyRalink=$phyName

		# assume only 1 desired netdev per phy with name scheme "wlanX"
		# ignore any monitor interface (assume "mon" in name)
		devRalink=$(ls "$debugfs/$phyName" | grep netdev:wlan | cut -d : -f 2)

		# TODO: renaming of netdev is problematic (driver-dependent)
#		if [[ "$devRalink" != "" ]] && [[ "$devRalink" != "wlan1" ]]; then
#			iw dev $devRalink del
#		fi

	fi

done

	# TODO: renaming of netdev is problematic (driver-dependent)
#	if [[ "$devAtheros" != "wlan0" ]]; then
#		devAtheros="wlan0"
#		iw $phyAtheros interface add $devAtheros type mp
#	fi

#	if [[ "$devRalink" != "wlan1" ]]; then
#		devRalink="wlan1"
#		iw $phyRalink interface add $devRalink type mp
#	fi

export PHYNAME_ATH=$phyAtheros
export DEVNAME_ATH=$devAtheros
export PHYNAME_RAL=$phyRalink
export DEVNAME_RAL=$devRalink

#echo ${PHYNAME_ATH}
#echo ${DEVNAME_ATH}
#echo ${PHYNAME_RAL}
#echo ${DEVNAME_RAL}

unset IFS

