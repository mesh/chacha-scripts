#!/bin/bash	

source /home/root/phyNameDetectAndDevRename.sh

myName=$(iam)
myNumber="${myName//Galileo/}"

case "$1" in		

"1") # Galileo 1
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:23 plink_action block  &
			;;
"2") # Galileo 2
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:20 plink_action block  &
			;;
"3") # Galileo 3
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0f plink_action block  &
			;;
"4") # Galileo 4
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4f plink_action block  &
			;;
"5") # Galileo 5
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:90 plink_action block  &
			;;
"6") # Galileo 6
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8a plink_action block  &
			;;
"7") # Galileo 7
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:18 plink_action block  &
			;;
"8") # Galileo 8
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2b plink_action block  &
			;;
"9") # Galileo 9
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:15 plink_action block  &
			;;
"10") # Galileo 10
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:57 plink_action block  &
			;;
"11") # Galileo 11
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:41 plink_action block  &
			;;
"12") # Galileo 12
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:71 plink_action block  &
			;;
"13") # Galileo 13
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:2a plink_action block  &
			;;
"14") # Galileo 14
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:62 plink_action block  &
			;;
"15") # Galileo 15
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:55 plink_action block  &
			;;
"16") # Galileo 16
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:8e plink_action block  &
			;;
"17") # Galileo 17
			iw dev ${DEVNAME_ATH} station set 04:f0:21:0f:77:54 plink_action block  &
			;;
"18") # Galileo 18
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:39 plink_action block  &
			;;
"19") # Galileo 19
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:33 plink_action block  &
			;;
"20") # Galileo 20
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:af plink_action block  &
			;;
"21") # Galileo 21
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:3f plink_action block  &
			;;
"22") # Galileo 22
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:51 plink_action block  &
			;;
"23") # Galileo 23
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:4c plink_action block  &
			;;
"24") # Galileo 24
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c2:88 plink_action block  &
			;;
"25") # Galileo 25
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0d plink_action block  &
			;;
"26") # Galileo 26
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:53 plink_action block  &
			;;
"27") # Galileo 27
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:52 plink_action block  &
			;;
"28") # Galileo 28
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:17 plink_action block  &
			;;
"29") # Galileo 29
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:51 plink_action block  &
			;;
"30") # Galileo 30
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:59 plink_action block  &
			;;
"31") # Galileo 31
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:52 plink_action block  &
			;;
"32") # Galileo 32
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:54 plink_action block  &
			;;
"33") # Galileo 33
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:38 plink_action block  &
			;;
"34") # Galileo 34
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:65 plink_action block  &
			;;
"35") # Galileo 35
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c3:21 plink_action block  &
			;;
"36") # Galileo 36
			iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c5:53 plink_action block  &
			;;

"40") # Galileo 40
			iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &
			;;
			*)
			;;

esac
