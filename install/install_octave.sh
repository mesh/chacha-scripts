#!/bin/bash

# "dpkg -l octave"	-> ...
# "which octave"	-> /usr/bin/octave

sudo add-apt-repository ppa:octave/stable
sudo apt update
sudo apt install octave octave-doc octave-htmldoc gnuplot epstool transfig
