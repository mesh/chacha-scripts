#!/bin/bash

# netcat client -> tcpserver (4 ports) test measurement for scenario "concurrent cluster data upload from 4 "aux" CH to 1 Master CH"

IF=1	# TODO: 1 -> Atheros mPCIe | 2 -> Ralink USB

if (( ${IF} == 1 )); then
	IP_PREFIX="192.168.123"
fi
if (( ${IF} == 2 )); then
	IP_PREFIX="192.168.124"
fi

# prepare nodes
echo "Prepare nodes ..."

if (( ${IF} == 1 )); then
	ssh root@"galileo15" "pkill tcpserver; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo8"  "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1250K count=1 > /run/TestData;" & # 4CM + self
	ssh root@"galileo10" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1250K count=1 > /run/TestData;" & # 4CM + self
	ssh root@"galileo20" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1500K count=1 > /run/TestData;" & # 5CM + self
	ssh root@"galileo22" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1000K count=1 > /run/TestData;" & # 3CM + self
fi
if (( ${IF} == 2 )); then
	INIT_IF2_PATH="/home/root/CHaChA_distributed/exec"
	ssh root@"galileo15" "pkill tcpserver; cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.24 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo8"  "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.17 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1250K count=1 > /run/TestData;" & # 4CM + self
	ssh root@"galileo10" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.19 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1250K count=1 > /run/TestData;" & # 4CM + self
	ssh root@"galileo20" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.29 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1500K count=1 > /run/TestData;" & # 5CM + self
	ssh root@"galileo22" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.31 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs; dd if=/dev/urandom bs=1000K count=1 > /run/TestData;" & # 3CM + self
fi

sleep 10 # TODO: give some time for preparation

# Master CH: start 4 tcpserver instances on different ports
echo "Start tcpserver instances on Master CH ..."
ssh root@"galileo15" "{ tcpserver -vXHlRD -c 1 -b 50 ${IP_PREFIX}.24 8080 bash -c 'cat >/dev/null' ; } 2>&1 | ts &>/run/logs/masterCH-G15_tcpserver_port8080.log &" &
ssh root@"galileo15" "{ tcpserver -vXHlRD -c 1 -b 50 ${IP_PREFIX}.24 8081 bash -c 'cat >/dev/null' ; } 2>&1 | ts &>/run/logs/masterCH-G15_tcpserver_port8081.log &" &
ssh root@"galileo15" "{ tcpserver -vXHlRD -c 1 -b 50 ${IP_PREFIX}.24 8082 bash -c 'cat >/dev/null' ; } 2>&1 | ts &>/run/logs/masterCH-G15_tcpserver_port8082.log &" &
ssh root@"galileo15" "{ tcpserver -vXHlRD -c 1 -b 50 ${IP_PREFIX}.24 8083 bash -c 'cat >/dev/null' ; } 2>&1 | ts &>/run/logs/masterCH-G15_tcpserver_port8083.log &" &

sleep 10 # TODO: delay burst start of "aux" CH

# "aux" CH: each start 1 netcat client TCP stream to exclusive Master CH server port (upload of [1+#CM]*TestDataSize, 5M in total @ 250K TestDataSize)
echo "Start netcat client instances on 'aux' CH ..."
ssh root@"galileo8"  "{ time nc -vv -n -q 0 ${IP_PREFIX}.24 8080 < /run/TestData &>/dev/null ; } 2>/run/logs/auxCH-G08_netcat_port8080.log &" &
ssh root@"galileo10" "{ time nc -vv -n -q 0 ${IP_PREFIX}.24 8081 < /run/TestData &>/dev/null ; } 2>/run/logs/auxCH-G10_netcat_port8081.log &" &
ssh root@"galileo20" "{ time nc -vv -n -q 0 ${IP_PREFIX}.24 8082 < /run/TestData &>/dev/null ; } 2>/run/logs/auxCH-G20_netcat_port8082.log &" &
ssh root@"galileo22" "{ time nc -vv -n -q 0 ${IP_PREFIX}.24 8083 < /run/TestData &>/dev/null ; } 2>/run/logs/auxCH-G22_netcat_port8083.log &" &

# continuously check for running netcat client instances on aux CH -> measurement done when all quit
LOGINS="root@galileo8,root@galileo10,root@galileo20,root@galileo22"
PROG="nc"
DONE=false
while [[ "$DONE" == false ]]; do
	sleep 20	# TODO
	echo "Check for running $PROG instances ..."
	output=$(parallel --gnu --no-notice --nonall -S $LOGINS "pgrep -x $PROG &>/dev/null && echo \"\$(iam)-1\" || echo \"\$(iam)-0\"")
	DONE=true
	for i in $output; do
		if [[ "$i" =~ "Galileo"[0-9]{1,2}"-1" ]]; then
			echo "${i::-2} not done"
			DONE=false
		else
			echo "${i::-2} done"
			DONE=$DONE
		fi
	done
done

# collect log files
echo "Done. Collect log files ..."
TIMESTAMP=$(date +%Y%m%d_%H-%M-%S)
mkdir -p logs/${TIMESTAMP}
rsync -a root@"galileo15":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo8":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo10":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo20":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo22":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &

sleep 5

# cleanup
ssh root@"galileo15" "pkill tcpserver; rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo8"  "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo10" "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo20" "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo22" "rm /run/logs/*.log &>/dev/null;" &

