#!/bin/bash

# iperf3 client -> iperf3 server test measurement for scenario "concurrent cluster data upload from 4 "aux" CH to 1 Master CH"

IF=1	# TODO: 1 -> Atheros mPCIe | 2 -> Ralink USB

if (( ${IF} == 1 )); then
	IP_PREFIX="192.168.123"
fi
if (( ${IF} == 2 )); then
	IP_PREFIX="192.168.124"
fi

# prepare nodes
echo "Prepare nodes ..."

if (( ${IF} == 1 )); then
	ssh root@"galileo15" "pkill iperf3; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo8"  "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo10" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo20" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo22" "rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
fi
if (( ${IF} == 2 )); then
	INIT_IF2_PATH="/home/root/CHaChA_distributed/exec"
	ssh root@"galileo15" "pkill iperf3; cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.24 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo8"  "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.17 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo10" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.19 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo20" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.29 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
	ssh root@"galileo22" "cd ${INIT_IF2_PATH} && ./initIF2.sh ${IP_PREFIX}.31 5745; rm /run/logs/*.log &>/dev/null; mkdir -p /run/logs;" &
fi

sleep 10 # TODO: give some time for preparation

# Master CH: start 4 iperf3 server instances on different ports
echo "Start iperf3 server instances on Master CH ..."
ssh root@"galileo15" "iperf3 -s -1 --bind ${IP_PREFIX}.24 -p 8080 -V -i 0.1 --forceflush | ts &>/run/logs/masterCH-G15_iperfServer_port8080.log &" &
ssh root@"galileo15" "iperf3 -s -1 --bind ${IP_PREFIX}.24 -p 8081 -V -i 0.1 --forceflush | ts &>/run/logs/masterCH-G15_iperfServer_port8081.log &" &
ssh root@"galileo15" "iperf3 -s -1 --bind ${IP_PREFIX}.24 -p 8082 -V -i 0.1 --forceflush | ts &>/run/logs/masterCH-G15_iperfServer_port8082.log &" &
ssh root@"galileo15" "iperf3 -s -1 --bind ${IP_PREFIX}.24 -p 8083 -V -i 0.1 --forceflush | ts &>/run/logs/masterCH-G15_iperfServer_port8083.log &" &

sleep 10 # TODO: delay burst start of "aux" CH

# "aux" CH: each start 1 iperf3 client TCP stream to exclusive Master CH server port (upload of [1+#CM]*TestDataSize, 5M in total @ 250K TestDataSize)
echo "Start iperf3 client instances on 'aux' CH ..."
ssh root@"galileo8"  "iperf3 -c ${IP_PREFIX}.24 -p 8080 -n 1.25M --bind ${IP_PREFIX}.17 -V -i 0.1 --forceflush | ts &>/run/logs/auxCH-G08_iperfClient_port8080.log &" & # 4CM + self
ssh root@"galileo10" "iperf3 -c ${IP_PREFIX}.24 -p 8081 -n 1.25M --bind ${IP_PREFIX}.19 -V -i 0.1 --forceflush | ts &>/run/logs/auxCH-G10_iperfClient_port8081.log &" & # 4CM + self
ssh root@"galileo20" "iperf3 -c ${IP_PREFIX}.24 -p 8082 -n 1.50M --bind ${IP_PREFIX}.29 -V -i 0.1 --forceflush | ts &>/run/logs/auxCH-G20_iperfClient_port8082.log &" & # 5CM + self
ssh root@"galileo22" "iperf3 -c ${IP_PREFIX}.24 -p 8083 -n 1.00M --bind ${IP_PREFIX}.31 -V -i 0.1 --forceflush | ts &>/run/logs/auxCH-G22_iperfClient_port8083.log &" & # 3CM + self

# continuously check for running iperf3 client instances on aux CH -> measurement done when all quit
LOGINS="root@galileo8,root@galileo10,root@galileo20,root@galileo22"
PROG="iperf3"
DONE=false
while [[ "$DONE" == false ]]; do
	sleep 20	# TODO
	echo "Check for running $PROG instances ..."
	output=$(parallel --gnu --no-notice --nonall -S $LOGINS "pgrep -x $PROG &>/dev/null && echo \"\$(iam)-1\" || echo \"\$(iam)-0\"")
	DONE=true
	for i in $output; do
		if [[ "$i" =~ "Galileo"[0-9]{1,2}"-1" ]]; then
			echo "${i::-2} not done"
			DONE=false
		else
			echo "${i::-2} done"
			DONE=$DONE
		fi
	done
done

# collect log files
echo "Done. Collect log files ..."
TIMESTAMP=$(date +%Y%m%d_%H-%M-%S)
mkdir -p logs/${TIMESTAMP}
rsync -a root@"galileo15":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo8":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo10":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo20":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &
rsync -a root@"galileo22":/run/logs/*.log	logs/${TIMESTAMP} &>/dev/null &

sleep 5

# cleanup
ssh root@"galileo15" "pkill iperf3; rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo8"  "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo10" "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo20" "rm /run/logs/*.log &>/dev/null;" &
ssh root@"galileo22" "rm /run/logs/*.log &>/dev/null;" &

