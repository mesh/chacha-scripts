#!/usr/bin/env bash

# TODO: define nodes of clustered network
#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

# source helper script to translate between node numbers and network addresses
# TODO: define path of address_list.sh
ADDRESSLIST_PATH="/home/superuser/Resilio Sync Tim Brockmann/CHaChA/analysis_new/pcap"
source "${ADDRESSLIST_PATH}"/address_list.sh
NODE_MAC=($NODE_MAC) # MAC info helper array

# remove any previous stats files
rm *.ods 2>/dev/null

# iterate over measurement run directories
firstDir=true
for dir in *; do

	if [[ -d "$dir" && ! -L "$dir" ]]; then

		# is a directory and no symlink

		# enter measurement run dir
		cd $dir

		NEWFILE_LIST=""

		# use tool "tshark" to create filtered pcap per node -> contains only frames where TXADDR=LOCAL_MAC
		# TODO: display filter switch "-Y" is valid only for newer tshark versions (old versions use "-R")
		for i in $LIST; do

			FILE=Node_$i/tcpdump_node_$i.pcap

			echo "$FILE"
			echo "${NODE_MAC[$(($i-1))]}"

            # TODO: forwarded UDP broadcast traffic (node is TA but not SA, DA is multicast, this traffic is not included in its own pcap) is taken from capture of one of its neighbors
            case "$i" in
            #
            "1") # G1->G2
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((2-1))]} && wlan.sa!=${NODE_MAC[$((2-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "2") # G2->G1
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((1-1))]} && wlan.sa!=${NODE_MAC[$((1-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "3") # G3->G4
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((4-1))]} && wlan.sa!=${NODE_MAC[$((4-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "4") # G4->G3
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((3-1))]} && wlan.sa!=${NODE_MAC[$((3-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "5") # TODO: G5->11
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((11-1))]} && wlan.sa!=${NODE_MAC[$((11-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            # TODO

            "7") # G7->G8
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((8-1))]} && wlan.sa!=${NODE_MAC[$((8-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "8") # G8->G7
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((7-1))]} && wlan.sa!=${NODE_MAC[$((7-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "9") # G9->G10
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((10-1))]} && wlan.sa!=${NODE_MAC[$((10-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "10") # G10->G9
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((9-1))]} && wlan.sa!=${NODE_MAC[$((9-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "11") # TODO: G11->G5
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((5-1))]} && wlan.sa!=${NODE_MAC[$((5-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            # TODO
            "13") # G13->G14
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((14-1))]} && wlan.sa!=${NODE_MAC[$((14-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "14") # G14->G13
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((13-1))]} && wlan.sa!=${NODE_MAC[$((13-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "15") # G15->G16
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((16-1))]} && wlan.sa!=${NODE_MAC[$((16-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "16") # G16->G15
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((15-1))]} && wlan.sa!=${NODE_MAC[$((15-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "17") # TODO: G17->G23
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((23-1))]} && wlan.sa!=${NODE_MAC[$((23-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            # TODO
            "19") # G19->G20
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((20-1))]} && wlan.sa!=${NODE_MAC[$((20-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "20") # G20->G19
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((19-1))]} && wlan.sa!=${NODE_MAC[$((19-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "21") # G21->G22
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((22-1))]} && wlan.sa!=${NODE_MAC[$((22-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "22") # G22->G21
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((21-1))]} && wlan.sa!=${NODE_MAC[$((21-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "23") # TODO: G23->G17
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((17-1))]} && wlan.sa!=${NODE_MAC[$((17-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            # TODO
            "25") # G25->G26
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((26-1))]} && wlan.sa!=${NODE_MAC[$((26-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "26") # G26->G25
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((25-1))]} && wlan.sa!=${NODE_MAC[$((25-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "27") # G27->G28
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((28-1))]} && wlan.sa!=${NODE_MAC[$((28-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "28") # TODO: G28->27+29
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]}) || (wlan.ta==${NODE_MAC[$((27-1))]} && wlan.sa!=${NODE_MAC[$((27-1))]} && wlan.da == ff:ff:ff:ff:ff:ff) || (wlan.ta==${NODE_MAC[$((29-1))]} && wlan.sa!=${NODE_MAC[$((29-1))]} && wlan.da == ff:ff:ff:ff:ff:ff)" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            "29") # TODO: G29->xx
                tshark -r $FILE -Y "(wlan.ta==${NODE_MAC[$(($i-1))]})" -w "Node_$i/tcpdump_filtered_node_$i.pcap"
                ;;
            # TODO
            #
            *)
            	;;
            #
            esac

			NEWFILE_LIST="Node_$i/tcpdump_filtered_node_$i.pcap $NEWFILE_LIST"

		done

		FILES=$NEWFILE_LIST
		echo "$FILES"

		# use tool "mergecap" to merge all nodes' capture files of this measurement run
        # TODO: use -a option to just append the non-overlapping capture files to one another and prevent timestamp based merge problems?
        #mergecap -a -w traffic_capture_merged.pcap $FILES
		mergecap -w traffic_capture_merged.pcap $FILES

		# extract TCP and UDP traffic statistics
		#
		# export this measurement run's I/O statistics as text files (tshark feature)
		tshark -r traffic_capture_merged.pcap -q -z io,stat,0,tcp.port==4710 > stats_tcp.txt
		tshark -r traffic_capture_merged.pcap -q -z io,stat,0,udp.port==4711 > stats_udp.txt
		#
		# store pcap files that only include CHaChA TCP/UDP traffic of this measurement run
		tshark -r traffic_capture_merged.pcap -Y "tcp.port==4710" -w traffic_capture_merged_tcp.pcap
		tshark -r traffic_capture_merged.pcap -Y "udp.port==4711" -w traffic_capture_merged_udp.pcap
		#
		# use tool "capinfos" on the reduced merged files to extract statistics as csv
		#		
		# capinfos [OPTIONS] <infile>
		# ============================================================================
		# -m	*	Separate the infos with comma (,) characters.
		# -Q	*	Quote infos with double quotes (").
		# -T	*	Generate a table report. (includes following display options)
		# -r	*	Do not generate header record.
		# ============================================================================
		# -c		Displays the number of packets in the capture file.
		# -d		Displays the total length of all packets in the file, in bytes.
		# -u		Displays the capture duration, in seconds.
		# -x		Displays the average packet rate, in packets/sec
		# -y		Displays the average data rate, in bytes/sec
		# -z		Displays the average packet size, in bytes
		#
		# skip column header generation for all report entries except the first
		options="-TmQr"
		if [[ "$firstDir" == true ]]; then
			firstDir=false
			options="-TmQ"
		fi
		# add this measurement run's statistics to "global" report
		capinfos $options traffic_capture_merged_tcp.pcap >> ../stats_tcp.ods
		capinfos $options traffic_capture_merged_udp.pcap >> ../stats_udp.ods

		# leave measurement run dir
		cd ..

	fi

done

