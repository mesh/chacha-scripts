#!/usr/bin/env python

# import libraries
import os
import re

# output data structures
output = {}
time_array = []

# scan through sub folders and files of current path
for (dirpath, dirnames, filenames) in os.walk('.'):
	for f in filenames:
		# check for time logs and remember durations ("real" value) in msecs
		if 'time_netcat_node' in str(f):
			e = os.path.join(str(dirpath), str(f)) 
			node=f[17:31]
			#print("\n"+node)
			txtfile = open(e, 'r')
			output[node] = []
			for line in txtfile:
				if 'real' in line:
					# example: "real	0m48.728s"
					elems=re.split(r'\t+', line)
					# extract string after "real\t" as time
					time=elems[1]
					#print(time)
					# calc duration in msec
					time_msec=int(time[time.find(".")+1:time.find("s")])
					#print(time_msec)
					time_sec=int(time[time.find("m")+1:time.find(".")])
					#print(time_sec)
					time_min=int(time[0:time.find("m")])
					#print(time_min)
					time_msec=time_msec+1000*time_sec+60*1000*time_min
					#print(time_msec)
					# append to results
					time_array.append(time_msec)
					output[node].append(str(time_msec)+'\n')

# calculate and output time results
if time_array:

	tabs = []
	for tab in output:
		tabs.append(tab)
	tabs.sort()

	output_file = open('minMaxAvgMonTime.txt', 'w')

	for tab in tabs:
		output_file.write(tab + '\t')
		for row in output[tab]:
		    output_file.write(row)

	output_file.write('\n')

	output_file.write('Max: '+str(max(time_array))+'\n')
	output_file.write('Min: '+str(min(time_array))+'\n')
	output_file.write('Avg: '+str(sum(time_array)/len(time_array))+'\n')
