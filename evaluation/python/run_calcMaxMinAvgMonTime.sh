#!/usr/bin/env bash

PYTHON_SCRIPT_PATH="/home/superuser/Resilio Sync Tim Brockmann/CHaChA/analysis_new/python"

# iterate over "Node_i" directories
for nodedir in *; do

	if [[ -d "$nodedir" && ! -L "$nodedir" ]]; then

		# is a directory and no symlink

		# enter node dir
		cd $nodedir

		# check for subdirs ("Run_j")
		subdircount=$(find "$PWD" -maxdepth 1 -type d | wc -l)

		if (($subdircount > 1)); then

			# has subdirs -> nodedir belongs to CH node

			echo "$nodedir"

			# iterate over "Run_j" directories
			for subdir in *; do

				if [[ -d "$subdir" && ! -L "$subdir" ]]; then

					# is a directory and no symlink

					echo "--> $subdir"

					cp "${PYTHON_SCRIPT_PATH}"/calcMaxMinAvgMonTime.py "$subdir"

					cd $subdir
					./calcMaxMinAvgMonTime.py
					cd ..

				fi

			done

		fi

		# leave node dir
		cd ..

	fi

done
